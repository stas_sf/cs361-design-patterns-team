﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetsCalculatorLib;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorInterface
{
    public partial class DeclareVariableForm : Form
    {
        public DeclareVariableForm()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            MainForm f = Owner as MainForm;
            this.Location = new Point(f.Left + 10, f.Top - 10);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            MainForm f = Owner as MainForm;
            string s = textBoxVarName.Text.ToLower();
            if ((s == null) || (s.Length == 0))
            {
                MessageBox.Show("Variable name cann't be empty.");
                return;
            }
            else
                if (f.ExistVariable(s))
                {
                    MessageBox.Show("There is such name. Variable name should be uniq.");
                    return;
                }
                else
                    if (!f.IsId(s))
                    {
                        MessageBox.Show("Variable name should contains only letters and begin from letter, but not from digit.");
                        return;
                    }
            f.variables.Add(new Variable(s));
            f.Refresh();
            f.EnableDeleteChange();
            this.Close();
        }
    }
}
