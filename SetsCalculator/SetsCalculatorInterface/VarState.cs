﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorInterface
{
    public class VarState
    {
        private Variable v;
        private int i;

        public VarState(Variable vv)
        {
            v = vv;
            i = 1;
        }

        public string Name { get { return v.Name; } }

        public int EntryNum { get { return i; } }

        public void IncEntryNum()
        {
            ++i;
        }

        public void DecEntryNum()
        {
            --i;
        }

        public string ToString()
        {
            return v.ToString();
        }

        public Variable GetVariable()
        {
            return v;
        }
    }
}
