﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetsCalculatorLib;
using SetsCalculatorLib.SimpleExpression;
using SetsCalculatorLib.Exceptions;
using SetsCalculatorLib.Operation;

namespace SetsCalculatorInterface
{
    public partial class MainForm : Form
    {
        public Expression currentExpression;
        /// <summary>
        /// Список, в котором xранятся переменные
        /// </summary>
        public List<Variable> variables = new List<Variable>();

        /// <summary>
        /// Цифры. С цифры не может начитнаться имя переменной
        /// </summary>
        List<char> digits = new List<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        /// <summary>
        /// Множество содержащее все сиволы, которые допустимо использовать в имени переменной
        /// </summary>
        HashSet<char> avaliableNameSymbols = new HashSet<char> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        /// <summary>
        /// Символы которые пропускаются при считывании множества
        /// </summary>
        private char[] skippedSymbols = { ',', ' ' };

        public List<VarState> varsInExpression = new List<VarState>();

        /// <summary>
        /// Функция для парсинга множества заданного в текстбоксе. Если встречается не число, то выбрасывается исключение
        /// </summary>
        /// <param name="setValue"></param>
        /// <returns></returns>
        public List<int> ParseSetValue(string setValue)
        {
            //Пустое множество задается [ ]
            if (setValue == "[]") return new List<int>();
            if ((setValue.Length < 2) || (setValue[0] != '[') || (setValue[setValue.Length - 1] != ']'))
                throw new System.ArgumentException();
            setValue = setValue.Substring(1, setValue.Length - 2);
            string[] setElements = setValue.Split(skippedSymbols, StringSplitOptions.RemoveEmptyEntries);
            List<int> nums = new List<int>();
            if (setElements.Length == 0)
                throw new System.ArgumentException();
            foreach (string elem in setElements)
            {
                int num;
                if (!int.TryParse(elem, out num) || num < 0)
                {
                    throw new System.ArgumentException();
                }
                nums.Add(num);
            }
            return nums;
        }

        public void Refresh()
        {
            comboBoxVariables.Items.Clear();
            comboBoxVariables1.Items.Clear();
            foreach (Variable v in variables)
            {
                comboBoxVariables.Items.Add(v.ToString());
                comboBoxVariables1.Items.Add(v.ToString());
            }
        }

        /// <summary>
        /// Функция проверки, подходит ли данная строка в качетсве идентификатора переменной
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool IsId(string varName)
        {
            if (varName == null)
                return false;
            for (int i = 0; i < varName.Length; ++i)
            {
                if (!(avaliableNameSymbols.Contains(varName[i])))
                    return false;
            }
            return (!digits.Contains(varName[0]));
        }

        /// <summary>
        /// Проверка существует ли уже переменная с таким именем
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public bool ExistVariable(string varName)
        {
            if (variables == null)
                return false;
            if (variables.Find((Variable v) => { return v.Name == varName; }) != null)
                return true;
            return false;
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonAddVariable_Click(object sender, EventArgs e)
        {
            InitVariableForm f = new InitVariableForm();
            f.Owner = this;
            f.ShowDialog();
        }

        protected void ClearExpression()
        {
            currentExpression = null;
            RefreshFormula();
        }

        private void buttonClearFormula_Click(object sender, EventArgs e)
        {
            ClearExpression();
        }

        private void buttonInitializeFormula_Click(object sender, EventArgs e)
        {
            InitExpressionForm f = new InitExpressionForm();
            f.Owner = this;
            f.ShowDialog();
        }

        private void buttonChangeVariable_Click(object sender, EventArgs e)
        {
            ChangeVariableForm f = new ChangeVariableForm();
            if (comboBoxVariables1.SelectedItem != null)
            {
                string varname = comboBoxVariables1.SelectedItem.ToString();
                f.v = variables.Find((Variable v) => { return v.Name == varname; });
                f.ShowDialog(this);
            }

        }

        /// <summary>
        /// Вспомогательная функция, делает доступными соответствующие элемены на panel2
        /// </summary>
        private void ActivateConstantVariable()
        {
            radioButtonConstant.Enabled = true;
            radioButtonVariable.Enabled = true;
            textBoxConstant.Enabled = true;
            comboBoxVariables.Enabled = true;
            textBoxSievingParam.Enabled = false;
            buttonExpandExpression.Enabled = true;
        }

        /// <summary>
        /// Вспомогательная функция, делает доступными соответствующие элемены на panel2
        /// </summary>
        private void ActivateSievingParam()
        {
            radioButtonConstant.Enabled = false;
            radioButtonVariable.Enabled = false;
            textBoxConstant.Enabled = false;
            comboBoxVariables.Enabled = false;
            textBoxSievingParam.Enabled = true;
            buttonExpandExpression.Enabled = true;
        }

        private RadioButton typeOfExpand = null;

        private void TypeOfExpand_change(object sender, EventArgs e)
        {
            if (radioButtonIntersection.Checked)
            {
                typeOfExpand = radioButtonIntersection;
                panel2.Enabled = true;
                ActivateConstantVariable();
                buttonExpandExpression.Enabled = true;
            }
            else
                if (radioButtonUnion.Checked)
                {
                    typeOfExpand = radioButtonUnion;
                    panel2.Enabled = true;
                    ActivateConstantVariable();
                }
                else
                    if (radioButtonDifference.Checked)
                    {
                        typeOfExpand = radioButtonDifference;
                        panel2.Enabled = true;
                        ActivateConstantVariable();
                    }
                    else
                        if (radioButtonSymmetricDifference.Checked)
                        {
                            typeOfExpand = radioButtonSymmetricDifference;
                            panel2.Enabled = true;
                            ActivateConstantVariable();
                        }
                        else
                            if (radioButtonSieving.Checked)
                            {
                                typeOfExpand = radioButtonSieving;
                                panel2.Enabled = true;
                                ActivateSievingParam();
                                textBoxSievingParam.Focus();
                            }
                            else
                            {
                                typeOfExpand = null;
                                panel2.Enabled = false;
                                buttonExpandExpression.Enabled = true;
                            }
        }

        private void buttonExpandFormula_Click(object sender, EventArgs e)
        {
            if (currentExpression == null)
                return;
            if (typeOfExpand == radioButtonSieving)
            {
                int n;
                if (int.TryParse(textBoxSievingParam.Text, out n) && n >= 0)
                {
                    currentExpression = new Sieving(currentExpression, n);
                    RefreshFormula();
                }
                else
                    MessageBox.Show("Enter not minus integer parameter");
            }
            else
            {
                SimpleExpression expr;
                string str;
                if (radioButtonConstant.Checked)
                {
                    try
                    {
                        List<int> l = ParseSetValue(textBoxConstant.Text);
                        expr = new Constant(l);
                    }
                    catch (ArgumentException err)
                    {
                        MessageBox.Show("Value should be empty or have a format: [value1,value2,...], where value1,value2,...>=0. To create empty set enter []");
                        return;
                    }
                }
                else //Выбрана переменная
                {
                    if (comboBoxVariables.SelectedItem == null) return;
                    str = comboBoxVariables.SelectedItem.ToString();
                    expr = variables.Find((Variable v) => { return v.Name == str; });
                    // Переменная добавляется в список переменных, используемых в формуле
                    Variable vv = expr as Variable;
                    if (vv != null)
                    {
                        int i = varsInExpression.FindLastIndex((VarState v) => { return v.Name == vv.Name; });
                        if (i == -1)
                            varsInExpression.Add(new VarState(vv));
                        else
                            varsInExpression[i].IncEntryNum();
                    }
                }
                if (typeOfExpand == radioButtonIntersection)
                    currentExpression = new Intersection(currentExpression, expr);
                else
                    if (typeOfExpand == radioButtonUnion)
                        currentExpression = new Union(currentExpression, expr);
                    else
                        if (typeOfExpand == radioButtonDifference)
                            currentExpression = new Difference(currentExpression, expr);
                        else
                            if (typeOfExpand == radioButtonSymmetricDifference)
                                currentExpression = new SymmetricDifference(currentExpression, expr);
                RefreshFormula();
            }
        }

        private void buttonDeclareVar_Click(object sender, EventArgs e)
        {
            DeclareVariableForm f = new DeclareVariableForm();
            f.ShowDialog(this);
        }

        private void comboBoxVariables1_SelectedValueChanged(object sender, EventArgs e)
        {
            string s = comboBoxVariables1.SelectedItem.ToString();
            Variable vv = variables.Find((Variable v) => { return v.Name == s; });
            try
            {
                textBoxVarValue.Text = vv.Evaluate().ToString();
            }
            catch (NullVariableValueException err)
            {}
            EnableDeleteChange();
        }

        public void EnableDeleteChange()
        {
            if (variables.Count > 0)
            {
                buttonDeleteVariable.Enabled = true;
                buttonChangeVariable.Enabled = true;
            }
            else
            {
                buttonDeleteVariable.Enabled = false;
                buttonChangeVariable.Enabled = false;
            }
        }

        private void buttonDeleteVariable_Click(object sender, EventArgs e)
        {
            if (comboBoxVariables1.SelectedItem != null)
            {
                string varname = comboBoxVariables1.SelectedItem.ToString();
                Variable vv = variables.Find((Variable v) => { return v.Name == varname; });
                if (vv != null)
                {
                    VarState vs = varsInExpression.Find((VarState vvs) => { return vvs.Name == vv.Name; });
                    if (vs != null)
                        ClearExpression();
                    variables.Remove(vv);
                    Refresh();
                    comboBoxVariables1.Text = "";
                }
                EnableDeleteChange();
            }
        }

        public void RefreshFormula()
        {
            if (currentExpression == null)
                textBoxCurrentExpression.Text = "";
            else
                textBoxCurrentExpression.Text = currentExpression.ToString();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxResult.Text = currentExpression.Evaluate().ToString();
            }
            catch (SetsCalculatorLib.Exceptions.NullVariableValueException ee)
            {
                MessageBox.Show("All variables should be initialized");
            }
            catch (NullReferenceException ee)
            { }
        }

        private void CheckVar(Expression expr)
        {
            Variable v = expr as Variable;
            if (v != null)
            {
                VarState vs = varsInExpression.Find((VarState vv) => { return v.Name == vv.Name; });
                if (vs != null)
                    if (vs.EntryNum > 1)
                        vs.DecEntryNum();
                    else
                        varsInExpression.Remove(vs);
            }
        }

        /// <summary>
        /// Откат последнего изменения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRollback_Click(object sender, EventArgs e)
        {
            if (currentExpression == null)
                return;
            BinaryOperation op = currentExpression as BinaryOperation;
            if (op != null)
            {
                var so = op.GetSecondOperand();
                CheckVar(so);
                currentExpression = op.GetFirstOperand();
            }
            else
            {
                SimpleExpression se = currentExpression as SimpleExpression;
                if (se != null)
                {
                    CheckVar(se);
                    currentExpression = null;
                }
                else
                {
                    UnaryOperation uop = (currentExpression as UnaryOperation);
                    if (uop != null)
                        currentExpression = uop.GetFirstOperand();
                }
            }
            RefreshFormula();
        }

        private void radioButtonConstant_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonConstant.Checked)
            {
                textBoxConstant.Focus();
            }
            else
                textBoxConstant.ResetText();
        }

        private void radioButtonVariable_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonVariable.Checked)
                comboBoxVariables.Focus();
            else
            {
                comboBoxVariables.SelectedItem = null;
                radioButtonConstant.Select();
                comboBoxVariables.ResetText();
            }
        }

        private void textBoxConstant_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBoxConstant_Click(object sender, EventArgs e)
        {
            radioButtonConstant.Checked = true;
        }

        private void comboBoxVariables_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioButtonVariable.Checked = true;
        }

        private void textBoxConstant_KeyPress(object sender, KeyPressEventArgs e)
        {
            radioButtonConstant.Checked = true;
        }

        private void SaveInFile(string filename, string data)
        {
            System.IO.File.WriteAllText(filename, data);
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                SaveInFile(saveFileDialog.FileName, currentExpression.ToRPNString());
            }
        }

        /// <summary>
        /// ДОбавление переменной в нужные структуры
        /// </summary>
        /// <param name="v"></param>
        private void AddVariable(Variable v)
        {
            if (v != null && !ExistVariable(v.Name))
            {
                VarState vs = varsInExpression.Find((VarState vv) => { return v.Name == vv.Name; });
                if (vs != null)
                    vs.IncEntryNum();
                else
                {
                    variables.Add(new Variable(v.Name));
                    varsInExpression.Add(new VarState(v));
                }
            }
        }

        /// <summary>
        /// Функция заполняет нужные структуры, читая выражение из строки
        /// </summary>
        /// <param name="expr"></param>
        public void LoadExpressionFromString(string expr)
        {
            /*currentExpression = Expression.ReadExprFromRPNString(expr);
            // когда выражение будет создано, нужно в интерфейсе отдельно сделать функцию,
            // которая заполнит список переменных, использованных переменных и количества вхождений.
            Variable spart = null;
            Expression fpart = currentExpression;            
            while (fpart != null)
            {
                if ((fpart as BinaryOperation) != null)
                {
                    // Если вторая часть (cамая внешняяя) выражения - переменная
                    spart = (fpart as BinaryOperation).GetSecondOperand() as Variable;
                    AddVariableFromExpression(spart);
                    fpart = (fpart as BinaryOperation).GetFirstOperand();
                }
                else
                {
                    if ((fpart as UnaryOperation) != null)
                        fpart = (fpart as UnaryOperation).GetFirstOperand();
                    else
                    {
                        AddVariableFromExpression(fpart as Variable);
                        fpart = null;
                    }
                }
            }*/

            //TEST
            string[] token = expr.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Stack<Expression> st = new Stack<Expression>();
            int n;
            for (int k = 0; k < token.Length; ++k)
            {
                if (token[k][0] == '[')
                {
                    // Будет изменяться счётчик цикла!
                    int km = k - 1;
                    HashSet<int> set = new HashSet<int>();
                    do
                    {
                        ++km;
                        string[] elems = token[km].Split(new Char[] { ',', ' ', ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string el in elems)
                        {
                            int i;
                            if (!int.TryParse(el, out i))
                            {
                                throw new FormatException("Неверный формат константы");
                            }
                            set.Add(i);
                        }
                    } while (token[km][token[km].Length - 1] != ']');

                    k = km;

                    st.Push(new Constant(set));
                }
                else if (token[k][0] == '$')
                {
                    string tmp = token[k].Remove(0, 1);
                    Variable v = new Variable(tmp);
                    AddVariable(v);
                    st.Push(variables.Find((Variable vv) => { return vv.Name == v.Name; }));
                }
                else if (token[k] == Constants.UNION)
                {
                    SimpleExpression r = st.Pop() as SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Union(l, r));
                }
                else if (token[k] == Constants.INTERSECTION)
                {
                    SimpleExpression r = st.Pop() as SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Intersection(l, r));
                }
                else if (token[k] == Constants.DIFFERENCE)
                {
                    SimpleExpression r = st.Pop() as SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Difference(l, r));
                }
                else if (token[k] == Constants.SYMMDIFF)
                {
                    SimpleExpression r = st.Pop() as SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new SymmetricDifference(l, r));
                }
                else if (int.TryParse(token[k], out n))
                {
                    int m = k + 1;

                    // Будем читать следующий токен!
                    ++k;

                    if (m < token.Length && token[k] == Constants.SIEVING)
                    {
                        Expression l = st.Pop();
                        st.Push(new Sieving(l, n));
                    }
                }
            }
            if (st.Count != 1)
                throw new InvalidOperationException();
            currentExpression = st.Pop();
        }

        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.Width >= 1000 && this.Height >= 500)
            {
                this.pictureBox1.Visible = true;
            }
            else
                this.pictureBox1.Visible = false;
        }
    }
}