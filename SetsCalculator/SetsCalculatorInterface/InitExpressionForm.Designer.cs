﻿namespace SetsCalculatorInterface
{
    partial class InitExpressionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.comboBoxVar = new System.Windows.Forms.ComboBox();
            this.radioButtonConstant = new System.Windows.Forms.RadioButton();
            this.textBoxConstant = new System.Windows.Forms.TextBox();
            this.radioButtonVar = new System.Windows.Forms.RadioButton();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.radioButtonFile = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(116, 141);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(197, 141);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // comboBoxVar
            // 
            this.comboBoxVar.FormattingEnabled = true;
            this.comboBoxVar.Location = new System.Drawing.Point(134, 63);
            this.comboBoxVar.Name = "comboBoxVar";
            this.comboBoxVar.Size = new System.Drawing.Size(150, 21);
            this.comboBoxVar.TabIndex = 2;
            this.comboBoxVar.SelectedIndexChanged += new System.EventHandler(this.comboBoxVar_SelectedIndexChanged);
            // 
            // radioButtonConstant
            // 
            this.radioButtonConstant.AutoSize = true;
            this.radioButtonConstant.Location = new System.Drawing.Point(8, 24);
            this.radioButtonConstant.Name = "radioButtonConstant";
            this.radioButtonConstant.Size = new System.Drawing.Size(120, 17);
            this.radioButtonConstant.TabIndex = 3;
            this.radioButtonConstant.TabStop = true;
            this.radioButtonConstant.Text = "Initialize as constant";
            this.radioButtonConstant.UseVisualStyleBackColor = true;
            // 
            // textBoxConstant
            // 
            this.textBoxConstant.Location = new System.Drawing.Point(134, 24);
            this.textBoxConstant.Name = "textBoxConstant";
            this.textBoxConstant.Size = new System.Drawing.Size(150, 20);
            this.textBoxConstant.TabIndex = 4;
            this.textBoxConstant.Click += new System.EventHandler(this.textBoxConstant_Click);
            // 
            // radioButtonVar
            // 
            this.radioButtonVar.AutoSize = true;
            this.radioButtonVar.Location = new System.Drawing.Point(8, 64);
            this.radioButtonVar.Name = "radioButtonVar";
            this.radioButtonVar.Size = new System.Drawing.Size(116, 17);
            this.radioButtonVar.TabIndex = 5;
            this.radioButtonVar.TabStop = true;
            this.radioButtonVar.Text = "Initialize as variable";
            this.radioButtonVar.UseVisualStyleBackColor = true;
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Location = new System.Drawing.Point(134, 90);
            this.textBoxFilePath.Multiline = true;
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(115, 45);
            this.textBoxFilePath.TabIndex = 6;
            this.textBoxFilePath.Click += new System.EventHandler(this.textBoxFilePath_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "Expression.txt";
            this.openFileDialog.Filter = "Text files|*.txt | All files | *.*";
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.Location = new System.Drawing.Point(255, 90);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(29, 45);
            this.buttonOpenFile.TabIndex = 7;
            this.buttonOpenFile.Text = "...";
            this.buttonOpenFile.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.buttonOpenFile.UseVisualStyleBackColor = true;
            this.buttonOpenFile.Click += new System.EventHandler(this.buttonOpenFile_Click);
            // 
            // radioButtonFile
            // 
            this.radioButtonFile.AutoSize = true;
            this.radioButtonFile.Location = new System.Drawing.Point(8, 102);
            this.radioButtonFile.Name = "radioButtonFile";
            this.radioButtonFile.Size = new System.Drawing.Size(88, 17);
            this.radioButtonFile.TabIndex = 8;
            this.radioButtonFile.TabStop = true;
            this.radioButtonFile.Text = "Load from file";
            this.radioButtonFile.UseVisualStyleBackColor = true;
            // 
            // InitExpressionForm
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 176);
            this.Controls.Add(this.radioButtonFile);
            this.Controls.Add(this.buttonOpenFile);
            this.Controls.Add(this.textBoxFilePath);
            this.Controls.Add(this.radioButtonVar);
            this.Controls.Add(this.textBoxConstant);
            this.Controls.Add(this.radioButtonConstant);
            this.Controls.Add(this.comboBoxVar);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Name = "InitExpressionForm";
            this.Text = "Initialize Expression";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ComboBox comboBoxVar;
        private System.Windows.Forms.RadioButton radioButtonConstant;
        private System.Windows.Forms.TextBox textBoxConstant;
        private System.Windows.Forms.RadioButton radioButtonVar;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonOpenFile;
        private System.Windows.Forms.RadioButton radioButtonFile;
    }
}