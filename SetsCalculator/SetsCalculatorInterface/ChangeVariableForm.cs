﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetsCalculatorLib;
using SetsCalculatorLib.SimpleExpression;
using SetsCalculatorLib.Exceptions;

namespace SetsCalculatorInterface
{
    public partial class ChangeVariableForm : Form
    {
        bool right = false;

        public Variable v;

        public ChangeVariableForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            MainForm f = this.Owner as MainForm;
            try
            {
                if (textBoxValue.Text == "")
                {        
                    throw new System.ArgumentException();
                }
                if (v == null)
                {
                    MessageBox.Show("Choose variable in main form");
                    return;
                }
                List<int> val = f.ParseSetValue(textBoxValue.Text);
                v.Initialize(val);
                //f.Refresh();
                this.Close();
            }
            catch (ArgumentException err)
            {
                MessageBox.Show("Value should have a format: [value1,value2,...], where value1,value2,...>=0. To create empty set enter []");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            MainForm f = Owner as MainForm;
            this.Location = new Point(f.Left + 10, f.Top - 10);
        }
    }
}
