﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetsCalculatorLib;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorInterface
{
    public partial class InitVariableForm : Form
    {
        public InitVariableForm()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            MainForm f = Owner as MainForm;
            string s = textBoxVarName.Text.ToLower();
            if ((s == null) || (s.Length == 0))
            {
                MessageBox.Show("Variable name cann't be empty.");
                return;
            }
            else
                if (f.ExistVariable(s))
                {
                    MessageBox.Show("There is such name. Variable name should be uniq.");
                    return;
                }
                else
                {
                    if (!f.IsId(s))
                    {
                        MessageBox.Show("Variable name should contains only letters and begin from letter, but not from digit.");
                        return;
                    }
                }
            if (textBoxVarValue.Text != "")
            {
                try
                {
                    List<int> value = f.ParseSetValue(textBoxVarValue.Text);
                    Variable v = new Variable(s);
                    v.Initialize(value);
                    f.variables.Add(v);
                    f.Refresh();
                    f.EnableDeleteChange();
                    this.Close();
                }
                catch (ArgumentException err)
                {
                    MessageBox.Show("Value should be empty or have a format: [value1,value2,...], where value1,value2,...>=0. To create empty set enter []");
                }
            }
            else // Если переменная не инициализируется сразу
            {
                MessageBox.Show("To init variable you should enter value");
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
           MainForm f = Owner as MainForm;
           this.Location = new Point(f.Left + 10, f.Top - 10);
        }
    }
}
