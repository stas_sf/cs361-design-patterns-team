﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SetsCalculatorLib;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorInterface
{
    public partial class InitExpressionForm : Form
    {
        private string expression;

        public InitExpressionForm()
        {
            InitializeComponent();
        }

        public void Focus()
        {
            textBoxConstant.Focus();
        }

        private void Form2_Load(object sender, EventArgs e)
        {          
            MainForm f = Owner as MainForm;
            this.Location = new Point(f.Left + 10, f.Top - 10);
            this.Focus();
            radioButtonConstant.Checked = true;
            if (f.variables.Count == 0)
            {
                radioButtonVar.Enabled = false;
                comboBoxVar.Enabled = false;

            }
            else
            {
                foreach (Variable v in f.variables)
                {
                    this.comboBoxVar.Items.Add(v.Name);
                }
            }
        }


        /*private void RadioButtonChange(object sender, EventArgs e)
        {
            if (radioButtonConstant.Checked)
                comboBoxVar.Enabled = false;
            if (radioButtonVar.Checked)
                textBoxConstant.Enabled = false;
        }*/

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            MainForm f = Owner as MainForm;
            if (radioButtonConstant.Checked)
            {
                try
                {
                    List<int> l = f.ParseSetValue(textBoxConstant.Text);
                    f.currentExpression = new Constant(l);
                    f.RefreshFormula();
                }
                catch (ArgumentException err)
                {
                    MessageBox.Show("Value should be empty or have a format: [value1,value2,...], where value1,value2,...>=0. To create empty set enter []");
                }
            }
            if (radioButtonVar.Checked)
            {
                string s = comboBoxVar.SelectedItem.ToString();
                if (s == "" || s == null)
                    MessageBox.Show("Choose variable");
                Variable vv = f.variables.Find((Variable v) => {return v.Name == s;});
                f.currentExpression = vv;
                f.varsInExpression.Add(new VarState(vv));
                f.RefreshFormula();
            }
            if (radioButtonFile.Checked)
            {
                if (expression == null) return;
                try
                {
                    f.varsInExpression.Clear();
                    f.variables.Clear();
                    f.LoadExpressionFromString(expression);                    
                    f.Refresh();
                    f.RefreshFormula();
                }
                catch (FormatException err)
                {
                    MessageBox.Show("Wrong recording of constant in file");
                }
                catch (InvalidOperationException err)
                {
                    MessageBox.Show("There is mistake in file");
                }
            }
            this.Close();
        }

        private void textBoxConstant_Click(object sender, EventArgs e)
        {
            radioButtonConstant.Checked = true;
        }

        private void comboBoxVar_SelectedIndexChanged(object sender, EventArgs e)
        {
            radioButtonVar.Checked = true;
        }

        private void textBoxFilePath_Click(object sender, EventArgs e)
        {
            radioButtonFile.Checked = true;
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            radioButtonFile.Checked = true;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                expression = System.IO.File.ReadAllText(openFileDialog.FileName,Encoding.Default);
                textBoxFilePath.Text = openFileDialog.FileName;
                
            }
        }
    }
}
