﻿namespace SetsCalculatorInterface
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonRollback = new System.Windows.Forms.Button();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.buttonInitializeFormula = new System.Windows.Forms.Button();
            this.buttonClearFormula = new System.Windows.Forms.Button();
            this.labelCurrentExpression = new System.Windows.Forms.Label();
            this.textBoxCurrentExpression = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelSievingParam = new System.Windows.Forms.Label();
            this.textBoxSievingParam = new System.Windows.Forms.TextBox();
            this.textBoxConstant = new System.Windows.Forms.TextBox();
            this.comboBoxVariables = new System.Windows.Forms.ComboBox();
            this.radioButtonConstant = new System.Windows.Forms.RadioButton();
            this.radioButtonVariable = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonSieving = new System.Windows.Forms.RadioButton();
            this.radioButtonSymmetricDifference = new System.Windows.Forms.RadioButton();
            this.radioButtonDifference = new System.Windows.Forms.RadioButton();
            this.radioButtonUnion = new System.Windows.Forms.RadioButton();
            this.radioButtonIntersection = new System.Windows.Forms.RadioButton();
            this.buttonExpandExpression = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonDeclareVar = new System.Windows.Forms.Button();
            this.comboBoxVariables1 = new System.Windows.Forms.ComboBox();
            this.textBoxVarValue = new System.Windows.Forms.TextBox();
            this.labelVariableValue = new System.Windows.Forms.Label();
            this.labelVariables = new System.Windows.Forms.Label();
            this.buttonChangeVariable = new System.Windows.Forms.Button();
            this.buttonDeleteVariable = new System.Windows.Forms.Button();
            this.buttonAddVariable = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(1, 2);
            this.splitContainer1.MinimumSize = new System.Drawing.Size(700, 330);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.buttonSave);
            this.splitContainer1.Panel1.Controls.Add(this.buttonRollback);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxResult);
            this.splitContainer1.Panel1.Controls.Add(this.buttonCalculate);
            this.splitContainer1.Panel1.Controls.Add(this.buttonInitializeFormula);
            this.splitContainer1.Panel1.Controls.Add(this.buttonClearFormula);
            this.splitContainer1.Panel1.Controls.Add(this.labelCurrentExpression);
            this.splitContainer1.Panel1.Controls.Add(this.textBoxCurrentExpression);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.buttonExpandExpression);
            this.splitContainer1.Panel2.Controls.Add(this.panel3);
            this.splitContainer1.Size = new System.Drawing.Size(701, 330);
            this.splitContainer1.SplitterDistance = 111;
            this.splitContainer1.TabIndex = 17;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(279, 66);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 29);
            this.buttonSave.TabIndex = 18;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonRollback
            // 
            this.buttonRollback.Location = new System.Drawing.Point(360, 30);
            this.buttonRollback.Name = "buttonRollback";
            this.buttonRollback.Size = new System.Drawing.Size(75, 29);
            this.buttonRollback.TabIndex = 18;
            this.buttonRollback.Text = "Rollback";
            this.buttonRollback.UseVisualStyleBackColor = true;
            this.buttonRollback.Click += new System.EventHandler(this.buttonRollback_Click);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxResult.Location = new System.Drawing.Point(455, 30);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(231, 44);
            this.textBoxResult.TabIndex = 17;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCalculate.Location = new System.Drawing.Point(526, 80);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(75, 23);
            this.buttonCalculate.TabIndex = 16;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // buttonInitializeFormula
            // 
            this.buttonInitializeFormula.Location = new System.Drawing.Point(279, 30);
            this.buttonInitializeFormula.Name = "buttonInitializeFormula";
            this.buttonInitializeFormula.Size = new System.Drawing.Size(75, 29);
            this.buttonInitializeFormula.TabIndex = 15;
            this.buttonInitializeFormula.Text = "Init";
            this.buttonInitializeFormula.UseVisualStyleBackColor = true;
            this.buttonInitializeFormula.Click += new System.EventHandler(this.buttonInitializeFormula_Click);
            // 
            // buttonClearFormula
            // 
            this.buttonClearFormula.Location = new System.Drawing.Point(360, 66);
            this.buttonClearFormula.Name = "buttonClearFormula";
            this.buttonClearFormula.Size = new System.Drawing.Size(75, 29);
            this.buttonClearFormula.TabIndex = 14;
            this.buttonClearFormula.Text = "Clear";
            this.buttonClearFormula.UseVisualStyleBackColor = true;
            this.buttonClearFormula.Click += new System.EventHandler(this.buttonClearFormula_Click);
            // 
            // labelCurrentExpression
            // 
            this.labelCurrentExpression.AutoSize = true;
            this.labelCurrentExpression.Location = new System.Drawing.Point(9, 14);
            this.labelCurrentExpression.Name = "labelCurrentExpression";
            this.labelCurrentExpression.Size = new System.Drawing.Size(95, 13);
            this.labelCurrentExpression.TabIndex = 11;
            this.labelCurrentExpression.Text = "Current Expression";
            // 
            // textBoxCurrentExpression
            // 
            this.textBoxCurrentExpression.Location = new System.Drawing.Point(9, 30);
            this.textBoxCurrentExpression.Multiline = true;
            this.textBoxCurrentExpression.Name = "textBoxCurrentExpression";
            this.textBoxCurrentExpression.ReadOnly = true;
            this.textBoxCurrentExpression.Size = new System.Drawing.Size(264, 65);
            this.textBoxCurrentExpression.TabIndex = 10;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel2.Controls.Add(this.labelSievingParam);
            this.panel2.Controls.Add(this.textBoxSievingParam);
            this.panel2.Controls.Add(this.textBoxConstant);
            this.panel2.Controls.Add(this.comboBoxVariables);
            this.panel2.Controls.Add(this.radioButtonConstant);
            this.panel2.Controls.Add(this.radioButtonVariable);
            this.panel2.Enabled = false;
            this.panel2.Location = new System.Drawing.Point(147, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(243, 126);
            this.panel2.TabIndex = 20;
            // 
            // labelSievingParam
            // 
            this.labelSievingParam.AutoSize = true;
            this.labelSievingParam.Location = new System.Drawing.Point(3, 98);
            this.labelSievingParam.Name = "labelSievingParam";
            this.labelSievingParam.Size = new System.Drawing.Size(75, 13);
            this.labelSievingParam.TabIndex = 5;
            this.labelSievingParam.Text = "Sieving Param";
            // 
            // textBoxSievingParam
            // 
            this.textBoxSievingParam.Location = new System.Drawing.Point(107, 95);
            this.textBoxSievingParam.Name = "textBoxSievingParam";
            this.textBoxSievingParam.Size = new System.Drawing.Size(121, 20);
            this.textBoxSievingParam.TabIndex = 4;
            // 
            // textBoxConstant
            // 
            this.textBoxConstant.Location = new System.Drawing.Point(107, 6);
            this.textBoxConstant.Name = "textBoxConstant";
            this.textBoxConstant.Size = new System.Drawing.Size(121, 20);
            this.textBoxConstant.TabIndex = 3;
            this.textBoxConstant.Click += new System.EventHandler(this.textBoxConstant_Click);
            this.textBoxConstant.TextChanged += new System.EventHandler(this.textBoxConstant_TextChanged);
            this.textBoxConstant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxConstant_KeyPress);
            // 
            // comboBoxVariables
            // 
            this.comboBoxVariables.FormattingEnabled = true;
            this.comboBoxVariables.Location = new System.Drawing.Point(107, 51);
            this.comboBoxVariables.Name = "comboBoxVariables";
            this.comboBoxVariables.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVariables.TabIndex = 2;
            this.comboBoxVariables.SelectedIndexChanged += new System.EventHandler(this.comboBoxVariables_SelectedIndexChanged);
            // 
            // radioButtonConstant
            // 
            this.radioButtonConstant.Location = new System.Drawing.Point(3, 6);
            this.radioButtonConstant.Name = "radioButtonConstant";
            this.radioButtonConstant.Size = new System.Drawing.Size(85, 17);
            this.radioButtonConstant.TabIndex = 0;
            this.radioButtonConstant.TabStop = true;
            this.radioButtonConstant.Text = "Constant";
            this.radioButtonConstant.UseVisualStyleBackColor = true;
            this.radioButtonConstant.CheckedChanged += new System.EventHandler(this.radioButtonConstant_CheckedChanged);
            // 
            // radioButtonVariable
            // 
            this.radioButtonVariable.AutoSize = true;
            this.radioButtonVariable.Location = new System.Drawing.Point(3, 52);
            this.radioButtonVariable.Name = "radioButtonVariable";
            this.radioButtonVariable.Size = new System.Drawing.Size(63, 17);
            this.radioButtonVariable.TabIndex = 0;
            this.radioButtonVariable.TabStop = true;
            this.radioButtonVariable.Text = "Variable";
            this.radioButtonVariable.UseVisualStyleBackColor = true;
            this.radioButtonVariable.CheckedChanged += new System.EventHandler(this.radioButtonVariable_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.radioButtonSieving);
            this.panel1.Controls.Add(this.radioButtonSymmetricDifference);
            this.panel1.Controls.Add(this.radioButtonDifference);
            this.panel1.Controls.Add(this.radioButtonUnion);
            this.panel1.Controls.Add(this.radioButtonIntersection);
            this.panel1.Location = new System.Drawing.Point(12, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(129, 126);
            this.panel1.TabIndex = 19;
            // 
            // radioButtonSieving
            // 
            this.radioButtonSieving.AutoSize = true;
            this.radioButtonSieving.Location = new System.Drawing.Point(3, 98);
            this.radioButtonSieving.Name = "radioButtonSieving";
            this.radioButtonSieving.Size = new System.Drawing.Size(60, 17);
            this.radioButtonSieving.TabIndex = 4;
            this.radioButtonSieving.TabStop = true;
            this.radioButtonSieving.Text = "Sieving";
            this.radioButtonSieving.UseVisualStyleBackColor = true;
            this.radioButtonSieving.CheckedChanged += new System.EventHandler(this.TypeOfExpand_change);
            // 
            // radioButtonSymmetricDifference
            // 
            this.radioButtonSymmetricDifference.AutoSize = true;
            this.radioButtonSymmetricDifference.Location = new System.Drawing.Point(3, 75);
            this.radioButtonSymmetricDifference.Name = "radioButtonSymmetricDifference";
            this.radioButtonSymmetricDifference.Size = new System.Drawing.Size(125, 17);
            this.radioButtonSymmetricDifference.TabIndex = 3;
            this.radioButtonSymmetricDifference.TabStop = true;
            this.radioButtonSymmetricDifference.Text = "Symmetric Difference";
            this.radioButtonSymmetricDifference.UseVisualStyleBackColor = true;
            this.radioButtonSymmetricDifference.CheckedChanged += new System.EventHandler(this.TypeOfExpand_change);
            // 
            // radioButtonDifference
            // 
            this.radioButtonDifference.AutoSize = true;
            this.radioButtonDifference.Location = new System.Drawing.Point(3, 52);
            this.radioButtonDifference.Name = "radioButtonDifference";
            this.radioButtonDifference.Size = new System.Drawing.Size(74, 17);
            this.radioButtonDifference.TabIndex = 2;
            this.radioButtonDifference.TabStop = true;
            this.radioButtonDifference.Text = "Difference";
            this.radioButtonDifference.UseVisualStyleBackColor = true;
            this.radioButtonDifference.CheckedChanged += new System.EventHandler(this.TypeOfExpand_change);
            // 
            // radioButtonUnion
            // 
            this.radioButtonUnion.AutoSize = true;
            this.radioButtonUnion.Location = new System.Drawing.Point(3, 29);
            this.radioButtonUnion.Name = "radioButtonUnion";
            this.radioButtonUnion.Size = new System.Drawing.Size(53, 17);
            this.radioButtonUnion.TabIndex = 1;
            this.radioButtonUnion.TabStop = true;
            this.radioButtonUnion.Text = "Union";
            this.radioButtonUnion.UseVisualStyleBackColor = true;
            this.radioButtonUnion.CheckedChanged += new System.EventHandler(this.TypeOfExpand_change);
            // 
            // radioButtonIntersection
            // 
            this.radioButtonIntersection.AutoSize = true;
            this.radioButtonIntersection.Location = new System.Drawing.Point(3, 6);
            this.radioButtonIntersection.Name = "radioButtonIntersection";
            this.radioButtonIntersection.Size = new System.Drawing.Size(80, 17);
            this.radioButtonIntersection.TabIndex = 0;
            this.radioButtonIntersection.TabStop = true;
            this.radioButtonIntersection.Text = "Intersection";
            this.radioButtonIntersection.UseVisualStyleBackColor = true;
            this.radioButtonIntersection.CheckedChanged += new System.EventHandler(this.TypeOfExpand_change);
            // 
            // buttonExpandExpression
            // 
            this.buttonExpandExpression.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExpandExpression.Enabled = false;
            this.buttonExpandExpression.Location = new System.Drawing.Point(12, 162);
            this.buttonExpandExpression.Name = "buttonExpandExpression";
            this.buttonExpandExpression.Size = new System.Drawing.Size(378, 23);
            this.buttonExpandExpression.TabIndex = 18;
            this.buttonExpandExpression.Text = "Expand Expression";
            this.buttonExpandExpression.UseVisualStyleBackColor = true;
            this.buttonExpandExpression.Click += new System.EventHandler(this.buttonExpandFormula_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.buttonDeclareVar);
            this.panel3.Controls.Add(this.comboBoxVariables1);
            this.panel3.Controls.Add(this.textBoxVarValue);
            this.panel3.Controls.Add(this.labelVariableValue);
            this.panel3.Controls.Add(this.labelVariables);
            this.panel3.Controls.Add(this.buttonChangeVariable);
            this.panel3.Controls.Add(this.buttonDeleteVariable);
            this.panel3.Controls.Add(this.buttonAddVariable);
            this.panel3.Location = new System.Drawing.Point(422, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 164);
            this.panel3.TabIndex = 17;
            // 
            // buttonDeclareVar
            // 
            this.buttonDeclareVar.Location = new System.Drawing.Point(133, 52);
            this.buttonDeclareVar.Name = "buttonDeclareVar";
            this.buttonDeclareVar.Size = new System.Drawing.Size(75, 23);
            this.buttonDeclareVar.TabIndex = 21;
            this.buttonDeclareVar.Text = "Declare";
            this.buttonDeclareVar.UseVisualStyleBackColor = true;
            this.buttonDeclareVar.Click += new System.EventHandler(this.buttonDeclareVar_Click);
            // 
            // comboBoxVariables1
            // 
            this.comboBoxVariables1.FormattingEnabled = true;
            this.comboBoxVariables1.Location = new System.Drawing.Point(6, 25);
            this.comboBoxVariables1.Name = "comboBoxVariables1";
            this.comboBoxVariables1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVariables1.TabIndex = 20;
            this.comboBoxVariables1.SelectedValueChanged += new System.EventHandler(this.comboBoxVariables1_SelectedValueChanged);
            // 
            // textBoxVarValue
            // 
            this.textBoxVarValue.Location = new System.Drawing.Point(14, 131);
            this.textBoxVarValue.Multiline = true;
            this.textBoxVarValue.Name = "textBoxVarValue";
            this.textBoxVarValue.ReadOnly = true;
            this.textBoxVarValue.Size = new System.Drawing.Size(100, 20);
            this.textBoxVarValue.TabIndex = 19;
            // 
            // labelVariableValue
            // 
            this.labelVariableValue.AutoSize = true;
            this.labelVariableValue.Location = new System.Drawing.Point(11, 115);
            this.labelVariableValue.Name = "labelVariableValue";
            this.labelVariableValue.Size = new System.Drawing.Size(75, 13);
            this.labelVariableValue.TabIndex = 18;
            this.labelVariableValue.Text = "Variable Value";
            // 
            // labelVariables
            // 
            this.labelVariables.AutoSize = true;
            this.labelVariables.Location = new System.Drawing.Point(9, 7);
            this.labelVariables.Name = "labelVariables";
            this.labelVariables.Size = new System.Drawing.Size(50, 13);
            this.labelVariables.TabIndex = 17;
            this.labelVariables.Text = "Variables";
            // 
            // buttonChangeVariable
            // 
            this.buttonChangeVariable.Enabled = false;
            this.buttonChangeVariable.Location = new System.Drawing.Point(133, 110);
            this.buttonChangeVariable.Name = "buttonChangeVariable";
            this.buttonChangeVariable.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeVariable.TabIndex = 16;
            this.buttonChangeVariable.Text = "Change";
            this.buttonChangeVariable.UseVisualStyleBackColor = true;
            this.buttonChangeVariable.Click += new System.EventHandler(this.buttonChangeVariable_Click);
            // 
            // buttonDeleteVariable
            // 
            this.buttonDeleteVariable.Enabled = false;
            this.buttonDeleteVariable.Location = new System.Drawing.Point(133, 81);
            this.buttonDeleteVariable.Name = "buttonDeleteVariable";
            this.buttonDeleteVariable.Size = new System.Drawing.Size(75, 23);
            this.buttonDeleteVariable.TabIndex = 15;
            this.buttonDeleteVariable.Text = "Delete";
            this.buttonDeleteVariable.UseVisualStyleBackColor = true;
            this.buttonDeleteVariable.Click += new System.EventHandler(this.buttonDeleteVariable_Click);
            // 
            // buttonAddVariable
            // 
            this.buttonAddVariable.Location = new System.Drawing.Point(133, 23);
            this.buttonAddVariable.Name = "buttonAddVariable";
            this.buttonAddVariable.Size = new System.Drawing.Size(75, 23);
            this.buttonAddVariable.TabIndex = 14;
            this.buttonAddVariable.Text = "Init";
            this.buttonAddVariable.UseVisualStyleBackColor = true;
            this.buttonAddVariable.Click += new System.EventHandler(this.buttonAddVariable_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "Expression";
            this.saveFileDialog.Filter = "Text files | *.txt";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::SetsCalculatorInterface.Properties.Resources.unicorn;
            this.pictureBox1.Location = new System.Drawing.Point(225, -50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 170);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 327);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(715, 365);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Button buttonInitializeFormula;
        private System.Windows.Forms.Button buttonClearFormula;
        private System.Windows.Forms.Label labelCurrentExpression;
        private System.Windows.Forms.TextBox textBoxCurrentExpression;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxSievingParam;
        private System.Windows.Forms.TextBox textBoxConstant;
        private System.Windows.Forms.ComboBox comboBoxVariables;
        private System.Windows.Forms.RadioButton radioButtonConstant;
        private System.Windows.Forms.RadioButton radioButtonVariable;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonSieving;
        private System.Windows.Forms.RadioButton radioButtonSymmetricDifference;
        private System.Windows.Forms.RadioButton radioButtonDifference;
        private System.Windows.Forms.RadioButton radioButtonUnion;
        private System.Windows.Forms.RadioButton radioButtonIntersection;
        private System.Windows.Forms.Button buttonExpandExpression;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxVarValue;
        private System.Windows.Forms.Label labelVariableValue;
        private System.Windows.Forms.Label labelVariables;
        private System.Windows.Forms.Button buttonChangeVariable;
        private System.Windows.Forms.Button buttonDeleteVariable;
        private System.Windows.Forms.Button buttonAddVariable;
        private System.Windows.Forms.ComboBox comboBoxVariables1;
        private System.Windows.Forms.Button buttonDeclareVar;
        private System.Windows.Forms.Button buttonRollback;
        private System.Windows.Forms.Label labelSievingParam;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

