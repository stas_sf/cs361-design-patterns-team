﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EratosthenesSieveSetLibrary
{
    /// <summary>
    /// Класс, содержащий множество простых чисел, не превосходящих заданное n
    /// </summary>
    public class EratosthenesSieveSet
    {
        private HashSet<int> primes;
        int n;

        public int N { get { return n; } }

        public EratosthenesSieveSet(int nn) 
        {
            n = nn;

            primes = new HashSet<int>();

            for (int i = 2; i < n + 1; ++i)
                primes.Add(i);

            for (int i = 2; i * i <= n; ++i)
                if (primes.Contains(i))
                    for (int j = i * i; j <= n; j += i)
                        primes.Remove(j);
        }

        public void RemoveNotPrimes(ref HashSet<int> hs)
        {
            HashSet<int> s = new HashSet<int>();
            foreach (int i in hs)
            {
                if (i >= n || primes.Contains(i))
                    s.Add(i);
            }
            hs = s;
        }
    }
}
