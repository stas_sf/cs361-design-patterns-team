﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SetsCalculatorLib.SimpleExpression
{
    /// <summary>
    /// Константа-множество
    /// </summary>
    public class Constant : SimpleExpression
    {
        /// <summary>
        /// Хеш-множество для хранения константы-множества
        /// </summary>
        private HashSet<int> set;

        public Constant(params int[] input)
        {
            set = new HashSet<int>();
            for (int i = 0; i < input.Length; ++i )
            {
                Debug.Assert(input[i] >= 0, "Значение должны быть неотрицательными!");
                set.Add(input[i]);
            }
        }

        public Constant(List<int> l)
        {
            set = new HashSet<int>();
            foreach (int i in l)
            {
                Debug.Assert(i >= 0, "Значение должны быть неотрицательными!");
                set.Add(i);
            }
        }

        public Constant(HashSet<int> hs)
        {
            set = new HashSet<int>();
            foreach (int i in hs)
                set.Add(i);
        }

        /// <summary>
        /// Вычисляет значение выражения
        /// </summary>
        /// <returns></returns>
        public override Constant Evaluate()
        {
            return (this);
        }

        /// <summary>
        /// Возвращает Хеш-множество, содержащее элементы константы
        /// </summary>
        /// <returns></returns>
        internal HashSet<int> GetSet()
        {
            return set;
        }

        /// <summary>
        /// Возвращает строковое представления выражения
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[");

            if (set.Count != 0)
            {
                foreach (int i in set)
                {
                    sb.AppendFormat("{0}, ", i);
                }

                sb.Remove(sb.Length - 2, 2);
            }
            sb.Append("]");

            return sb.ToString();
        }

        /// <summary>
        /// Возвращает строковое представление выражения в обратной польской записи
        /// </summary>
        /// <returns></returns>
        public override string ToRPNString()
        {
            return this.ToString();
        }
    }
}
