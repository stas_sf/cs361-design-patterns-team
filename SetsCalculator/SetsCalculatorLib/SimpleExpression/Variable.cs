﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using SetsCalculatorLib.Exceptions;

namespace SetsCalculatorLib.SimpleExpression
{
    /// <summary>
    /// Переменная-множество
    /// </summary>
    public class Variable : SimpleExpression
    {

        private string name;

        private Constant value;

        public Variable(string name)
        {
            this.name = name;
            this.value = null;
        }

        public Variable(string name, Constant c)
        {
            this.name = name;
            this.Initialize(c);
        }

        public Variable(string name, params int[] input)
        {
            this.name = name;
            this.Initialize(input);
        }

        public string Name { get { return name; } }

        void Initialize(Constant c)
        {
            value = c;
        }

        public void Initialize(params int[] input)
        {
            Constant c = new Constant(input);
            Initialize(c);
        }

        public void Initialize(List<int> l)
        {
            Constant c = new Constant(l);
            Initialize(c);
        }

        public override Constant Evaluate()
        {
            if (value == null)
                throw new NullVariableValueException("Значение меременной должно быть инициализированно!");
            return value;
        }

        /// <summary>
        /// Возвращает строковое представление выражения
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return name;
        }

        /// <summary>
        /// Возвращает строковое представление выражения в обратной польской записи
        /// </summary>
        /// <returns></returns>
        public override string ToRPNString()
        {
            return "$" + this.ToString();
        }
    }
}
