﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetsCalculatorLib.SimpleExpression;


namespace SetsCalculatorLib.Operation
{
    public class Union: BinOpWithSimpleSndOperand
    {
        /// <summary>
        /// Объединение множеств
        /// </summary>
        /// <param name="левый операнд"></param>
        /// <param name="правый операнд"></param>
        public Union(Expression first, SimpleExpression.SimpleExpression second) : base(first, second) { }

        /// <summary>
        /// Вычисляет значение выражения
        /// </summary>
        public override SimpleExpression.Constant Evaluate()
        {
            HashSet<int> hs = firstOperand.Evaluate().GetSet();
            hs.UnionWith(secondOperand.Evaluate().GetSet());
            return new Constant(hs);
        }

        /// <summary>
        /// Возвращает строковое представления выражения
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format(Constants.BRACK_BIN_OPERATION_FORMAT, firstOperand.ToString(), Constants.UNION, secondOperand.ToString());
        }

        /// <summary>
        /// Возвращает строковое представление выражения в обратной польской записи
        /// </summary>
        /// <returns></returns>
        public override string ToRPNString()
        {
            return String.Format(Constants.NO_BRACK_BIN_OPERATION_FORMAT, firstOperand.ToRPNString(), secondOperand.ToRPNString(), Constants.UNION);
        }
    }
}
