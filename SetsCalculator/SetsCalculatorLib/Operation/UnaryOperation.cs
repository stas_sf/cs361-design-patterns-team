﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetsCalculatorLib.Operation
{
    abstract public class UnaryOperation: Operation
    {
        public UnaryOperation(Expression expr)
        {
            firstOperand = expr;
        }
    }
}
