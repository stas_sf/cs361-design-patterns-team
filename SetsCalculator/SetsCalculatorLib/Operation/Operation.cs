﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetsCalculatorLib.Operation
{
    /// <summary>
    /// Абстрактная операция
    /// </summary>
    abstract public class Operation: Expression
    {
        /// <summary>
        /// Операнд операции
        /// </summary>
        protected Expression firstOperand;

        /// <summary>
        /// Возвращает ссылку на левый операнд выражения
        /// </summary>
        /// <returns></returns>
        public Expression GetFirstOperand()
        {
            return firstOperand;
        }
    }
}
