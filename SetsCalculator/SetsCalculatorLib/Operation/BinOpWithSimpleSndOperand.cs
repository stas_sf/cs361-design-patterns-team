﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorLib.Operation
{
    abstract public class BinOpWithSimpleSndOperand: BinaryOperation
    {
        public BinOpWithSimpleSndOperand(Expression first, SimpleExpression.SimpleExpression second): base(first, second) {}
    }
}
