﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EratosthenesSieveSetLibrary;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorLib.Operation
{
    public class Sieving : UnaryOperation
    {
        private EratosthenesSieveSet ess;

        public Sieving(Expression operand, int n): base(operand)
        {
            ess = new EratosthenesSieveSet(n);
        }

        /// <summary>
        /// Вычисляет значение выражения
        /// </summary>
        public override SimpleExpression.Constant Evaluate()
        {
            Constant c = firstOperand.Evaluate() as Constant;
            HashSet<int> hs = c.GetSet();
            ess.RemoveNotPrimes(ref hs);
            c = new Constant(hs);
            return c.Evaluate();
        }

        /// <summary>
        /// Возвращает строковое представления выражения
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {

            return String.Format(Constants.BRACK_BIN_OPERATION_FORMAT, firstOperand.ToString(), Constants.SIEVING, ess.N);
        }

        /// <summary>
        /// Возвращает строковое представление выражения в обратной польской записи
        /// </summary>
        /// <returns></returns>
        public override string ToRPNString()
        {
            return String.Format(Constants.NO_BRACK_BIN_OPERATION_FORMAT, firstOperand.ToRPNString(), ess.N, Constants.SIEVING);
        }
    }
}
