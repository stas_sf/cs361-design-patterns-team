﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetsCalculatorLib.SimpleExpression;

namespace SetsCalculatorLib.Operation
{
    abstract public class BinaryOperation: Operation
    {
        /// <summary>
        /// Правый операнд операции
        /// </summary>
        protected Expression secondOperand;

        public BinaryOperation(Expression first, Expression second)
        {
            firstOperand = first;
            secondOperand = second;
        }

        /// <summary>
        /// Возвращает ссылку на правый операнд выражения
        /// </summary>
        /// <returns></returns>
        public Expression GetSecondOperand()
        {
            return secondOperand;
        }
    }
}
