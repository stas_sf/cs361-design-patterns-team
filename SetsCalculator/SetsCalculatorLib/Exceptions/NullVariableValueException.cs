﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetsCalculatorLib.Exceptions
{
    /// <summary>
    /// Значение переменной-множества не инициализированно
    /// </summary>
    public class NullVariableValueException : Exception
    {
        public NullVariableValueException()
    {
    }

    public NullVariableValueException(string message)
        : base(message)
    {
    }

    public NullVariableValueException(string message, Exception inner)
        : base(message, inner)
    {
    }
    }
}
