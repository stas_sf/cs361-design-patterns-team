﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SetsCalculatorLib.SimpleExpression;
using SetsCalculatorLib.Operation;

namespace SetsCalculatorLib
{
    abstract public class Expression
    {
        /// <summary>
        /// Вычисляет значение выражения
        /// </summary>
        public abstract SimpleExpression.Constant Evaluate();

        /// <summary>
        /// Возвращает строковое представления выражения
        /// </summary>
        /// <returns></returns>
        public new abstract string ToString();

        /// <summary>
        /// Возвращает строковое представление выражения в обратной польской записи
        /// </summary>
        /// <returns></returns>
        public abstract string ToRPNString();


        /*public static Expression ReadExprFromRPNString(string rpn)
        {
            string[] token = rpn.Split(new Char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Stack<Expression> st = new Stack<Expression>();
            int n;
            for (int k = 0; k < token.Length; ++k)
            {
                if (token[k][0] == '[')
                {
                    // Будет изменяться счётчик цикла!
                    int km = k - 1;
                    HashSet<int> set = new HashSet<int>();
                    do
                    {
                        ++km;
                        string[] elems = token[km].Split(new Char[] { ',', ' ', ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string el in elems)
                        {
                            int i;
                            if (!int.TryParse(el, out i))
                            {
                                throw new FormatException("Неверный формат константы");
                            }
                            set.Add(i);
                        }
                    } while (token[km][token[km].Length - 1] != ']');

                    k = km;

                    st.Push(new Constant(set));
                }
                else if (token[k][0] == '$')
                {
                    string tmp = token[k].Remove(0, 1);
                    st.Push(new Variable(tmp));
                }
                else if (token[k] == Constants.UNION)
                {
                    SimpleExpression.SimpleExpression r = st.Pop() as SimpleExpression.SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Union(l, r));
                }
                else if (token[k] == Constants.INTERSECTION)
                {
                    SimpleExpression.SimpleExpression r = st.Pop() as SimpleExpression.SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Intersection(l, r));
                }
                else if (token[k] == Constants.DIFFERENCE)
                {
                    SimpleExpression.SimpleExpression r = st.Pop() as SimpleExpression.SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new Difference(l, r));
                }
                else if (token[k] == Constants.SYMMDIFF)
                {
                    SimpleExpression.SimpleExpression r = st.Pop() as SimpleExpression.SimpleExpression;
                    Expression l = st.Pop();
                    st.Push(new SymmetricDifference(l, r));
                }
                else if (int.TryParse(token[k], out n))
                {
                    int m = k + 1;

                    // Будем читать следующий токен!
                    ++k;

                    if (m < token.Length && token[k] == Constants.SIEVING)
                    {
                        Expression l = st.Pop();
                        st.Push(new Sieving(l, n));
                    }
                }
            }
            if (st.Count != 1)
                throw new InvalidOperationException();
            return st.Pop();
        }
        */
    }
}
