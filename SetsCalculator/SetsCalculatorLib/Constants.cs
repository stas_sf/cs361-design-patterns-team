﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetsCalculatorLib
{
    public class Constants
    {
        public const string UNION = "+";
        public const string INTERSECTION = "*";
        public const string DIFFERENCE = "\\";
        public const string SYMMDIFF = "-";
        public const string SIEVING = "s";
        public const string BRACK_BIN_OPERATION_FORMAT = "({0} {1} {2})";
        public const string NO_BRACK_BIN_OPERATION_FORMAT = "{0} {1} {2}";
    }
}
