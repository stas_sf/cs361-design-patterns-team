﻿namespace VirtualMemoryInterface
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.buttonFile = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxRandomFaults = new System.Windows.Forms.TextBox();
            this.textBoxClockFaults = new System.Windows.Forms.TextBox();
            this.textBoxLRUFaults = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.buttonTest = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonInit = new System.Windows.Forms.Button();
            this.textBoxPrMemSz = new System.Windows.Forms.TextBox();
            this.textBoxMemSz = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonLRU = new System.Windows.Forms.RadioButton();
            this.radioButtonClock = new System.Windows.Forms.RadioButton();
            this.radioButtonRandom = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonClear = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonHandle = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxAllAccess = new System.Windows.Forms.TextBox();
            this.textBoxFaults = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPages = new System.Windows.Forms.TextBox();
            this.buttonGenSeverlaPages = new System.Windows.Forms.Button();
            this.buttonGenOnePage = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.textBoxResults = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonClr = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.buttonClr);
            this.splitContainer.Panel1.Controls.Add(this.buttonFile);
            this.splitContainer.Panel1.Controls.Add(this.panel3);
            this.splitContainer.Panel1.Controls.Add(this.panel2);
            this.splitContainer.Panel1.Controls.Add(this.panel1);
            this.splitContainer.Panel1.Controls.Add(this.buttonClear);
            this.splitContainer.Panel1.Controls.Add(this.label5);
            this.splitContainer.Panel1.Controls.Add(this.buttonHandle);
            this.splitContainer.Panel1.Controls.Add(this.label1);
            this.splitContainer.Panel1.Controls.Add(this.textBoxAllAccess);
            this.splitContainer.Panel1.Controls.Add(this.textBoxFaults);
            this.splitContainer.Panel1.Controls.Add(this.label3);
            this.splitContainer.Panel1.Controls.Add(this.textBoxPages);
            this.splitContainer.Panel1.Controls.Add(this.buttonGenSeverlaPages);
            this.splitContainer.Panel1.Controls.Add(this.buttonGenOnePage);
            this.splitContainer.Panel1.Controls.Add(this.shapeContainer1);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.textBoxResults);
            this.splitContainer.Panel2.Controls.Add(this.label4);
            this.splitContainer.Size = new System.Drawing.Size(592, 386);
            this.splitContainer.SplitterDistance = 368;
            this.splitContainer.TabIndex = 0;
            // 
            // buttonFile
            // 
            this.buttonFile.Location = new System.Drawing.Point(269, 57);
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.Size = new System.Drawing.Size(79, 23);
            this.buttonFile.TabIndex = 25;
            this.buttonFile.Text = "Load from file";
            this.buttonFile.UseVisualStyleBackColor = true;
            this.buttonFile.Click += new System.EventHandler(this.buttonFile_Click);
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel3.Controls.Add(this.textBoxRandomFaults);
            this.panel3.Controls.Add(this.textBoxClockFaults);
            this.panel3.Controls.Add(this.textBoxLRUFaults);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.buttonTest);
            this.panel3.Location = new System.Drawing.Point(12, 318);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(332, 56);
            this.panel3.TabIndex = 24;
            // 
            // textBoxRandomFaults
            // 
            this.textBoxRandomFaults.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.textBoxRandomFaults.Location = new System.Drawing.Point(244, 25);
            this.textBoxRandomFaults.Name = "textBoxRandomFaults";
            this.textBoxRandomFaults.ReadOnly = true;
            this.textBoxRandomFaults.Size = new System.Drawing.Size(76, 20);
            this.textBoxRandomFaults.TabIndex = 6;
            // 
            // textBoxClockFaults
            // 
            this.textBoxClockFaults.Location = new System.Drawing.Point(161, 25);
            this.textBoxClockFaults.Name = "textBoxClockFaults";
            this.textBoxClockFaults.ReadOnly = true;
            this.textBoxClockFaults.Size = new System.Drawing.Size(76, 20);
            this.textBoxClockFaults.TabIndex = 5;
            // 
            // textBoxLRUFaults
            // 
            this.textBoxLRUFaults.Location = new System.Drawing.Point(75, 25);
            this.textBoxLRUFaults.Name = "textBoxLRUFaults";
            this.textBoxLRUFaults.ReadOnly = true;
            this.textBoxLRUFaults.Size = new System.Drawing.Size(77, 20);
            this.textBoxLRUFaults.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(81, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "LRU Faults";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(162, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "ClockFaults";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(244, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Random Faults";
            // 
            // buttonTest
            // 
            this.buttonTest.Location = new System.Drawing.Point(8, 9);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(59, 40);
            this.buttonTest.TabIndex = 0;
            this.buttonTest.Text = "Start Testing";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.buttonInit);
            this.panel2.Controls.Add(this.textBoxPrMemSz);
            this.panel2.Controls.Add(this.textBoxMemSz);
            this.panel2.Location = new System.Drawing.Point(181, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(163, 120);
            this.panel2.TabIndex = 23;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Primary memory size";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Memory size";
            // 
            // buttonInit
            // 
            this.buttonInit.Location = new System.Drawing.Point(6, 87);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(148, 23);
            this.buttonInit.TabIndex = 18;
            this.buttonInit.Text = "Init New Memory";
            this.buttonInit.UseVisualStyleBackColor = true;
            this.buttonInit.Click += new System.EventHandler(this.buttonInit_Click);
            // 
            // textBoxPrMemSz
            // 
            this.textBoxPrMemSz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPrMemSz.Location = new System.Drawing.Point(49, 22);
            this.textBoxPrMemSz.Name = "textBoxPrMemSz";
            this.textBoxPrMemSz.Size = new System.Drawing.Size(105, 20);
            this.textBoxPrMemSz.TabIndex = 19;
            // 
            // textBoxMemSz
            // 
            this.textBoxMemSz.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMemSz.Location = new System.Drawing.Point(49, 61);
            this.textBoxMemSz.Name = "textBoxMemSz";
            this.textBoxMemSz.Size = new System.Drawing.Size(105, 20);
            this.textBoxMemSz.TabIndex = 20;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.radioButtonLRU);
            this.panel1.Controls.Add(this.radioButtonClock);
            this.panel1.Controls.Add(this.radioButtonRandom);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(163, 120);
            this.panel1.TabIndex = 17;
            // 
            // radioButtonLRU
            // 
            this.radioButtonLRU.AutoSize = true;
            this.radioButtonLRU.Location = new System.Drawing.Point(17, 25);
            this.radioButtonLRU.Name = "radioButtonLRU";
            this.radioButtonLRU.Size = new System.Drawing.Size(47, 17);
            this.radioButtonLRU.TabIndex = 1;
            this.radioButtonLRU.TabStop = true;
            this.radioButtonLRU.Text = "LRU";
            this.radioButtonLRU.UseVisualStyleBackColor = true;
            this.radioButtonLRU.CheckedChanged += new System.EventHandler(this.radioButtonRandom_CheckedChanged);
            // 
            // radioButtonClock
            // 
            this.radioButtonClock.AutoSize = true;
            this.radioButtonClock.Location = new System.Drawing.Point(17, 71);
            this.radioButtonClock.Name = "radioButtonClock";
            this.radioButtonClock.Size = new System.Drawing.Size(52, 17);
            this.radioButtonClock.TabIndex = 3;
            this.radioButtonClock.TabStop = true;
            this.radioButtonClock.Text = "Clock";
            this.radioButtonClock.UseVisualStyleBackColor = true;
            this.radioButtonClock.CheckedChanged += new System.EventHandler(this.radioButtonRandom_CheckedChanged);
            // 
            // radioButtonRandom
            // 
            this.radioButtonRandom.AutoSize = true;
            this.radioButtonRandom.Location = new System.Drawing.Point(17, 48);
            this.radioButtonRandom.Name = "radioButtonRandom";
            this.radioButtonRandom.Size = new System.Drawing.Size(65, 17);
            this.radioButtonRandom.TabIndex = 2;
            this.radioButtonRandom.TabStop = true;
            this.radioButtonRandom.Text = "Random";
            this.radioButtonRandom.UseVisualStyleBackColor = true;
            this.radioButtonRandom.CheckedChanged += new System.EventHandler(this.radioButtonRandom_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Choose Algorithm";
            // 
            // buttonClear
            // 
            this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClear.Location = new System.Drawing.Point(198, 239);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(143, 32);
            this.buttonClear.TabIndex = 16;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(217, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Faults";
            // 
            // buttonHandle
            // 
            this.buttonHandle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonHandle.Location = new System.Drawing.Point(16, 239);
            this.buttonHandle.Name = "buttonHandle";
            this.buttonHandle.Size = new System.Drawing.Size(145, 32);
            this.buttonHandle.TabIndex = 4;
            this.buttonHandle.Text = "Start Handling";
            this.buttonHandle.UseVisualStyleBackColor = true;
            this.buttonHandle.Click += new System.EventHandler(this.buttonHandle_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 284);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "All memory access";
            // 
            // textBoxAllAccess
            // 
            this.textBoxAllAccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxAllAccess.Location = new System.Drawing.Point(109, 281);
            this.textBoxAllAccess.Name = "textBoxAllAccess";
            this.textBoxAllAccess.ReadOnly = true;
            this.textBoxAllAccess.Size = new System.Drawing.Size(100, 20);
            this.textBoxAllAccess.TabIndex = 6;
            // 
            // textBoxFaults
            // 
            this.textBoxFaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxFaults.Location = new System.Drawing.Point(259, 281);
            this.textBoxFaults.Name = "textBoxFaults";
            this.textBoxFaults.ReadOnly = true;
            this.textBoxFaults.Size = new System.Drawing.Size(100, 20);
            this.textBoxFaults.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Numbers of pages";
            // 
            // textBoxPages
            // 
            this.textBoxPages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPages.Location = new System.Drawing.Point(12, 31);
            this.textBoxPages.Name = "textBoxPages";
            this.textBoxPages.Size = new System.Drawing.Size(328, 20);
            this.textBoxPages.TabIndex = 1;
            // 
            // buttonGenSeverlaPages
            // 
            this.buttonGenSeverlaPages.Location = new System.Drawing.Point(132, 57);
            this.buttonGenSeverlaPages.Name = "buttonGenSeverlaPages";
            this.buttonGenSeverlaPages.Size = new System.Drawing.Size(131, 23);
            this.buttonGenSeverlaPages.TabIndex = 3;
            this.buttonGenSeverlaPages.Text = "Generate several pages";
            this.buttonGenSeverlaPages.UseVisualStyleBackColor = true;
            this.buttonGenSeverlaPages.Click += new System.EventHandler(this.buttonGenSeverlaPages_Click);
            // 
            // buttonGenOnePage
            // 
            this.buttonGenOnePage.Location = new System.Drawing.Point(11, 57);
            this.buttonGenOnePage.Name = "buttonGenOnePage";
            this.buttonGenOnePage.Size = new System.Drawing.Size(115, 23);
            this.buttonGenOnePage.TabIndex = 2;
            this.buttonGenOnePage.Text = "Generate one page";
            this.buttonGenOnePage.UseVisualStyleBackColor = true;
            this.buttonGenOnePage.Click += new System.EventHandler(this.buttonGenOnePage_Click);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(368, 386);
            this.shapeContainer1.TabIndex = 15;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape3.BorderWidth = 3;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 10;
            this.lineShape3.X2 = 340;
            this.lineShape3.Y1 = 305;
            this.lineShape3.Y2 = 305;
            // 
            // lineShape2
            // 
            this.lineShape2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape2.BorderWidth = 3;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 10;
            this.lineShape2.X2 = 340;
            this.lineShape2.Y1 = 228;
            this.lineShape2.Y2 = 228;
            // 
            // lineShape1
            // 
            this.lineShape1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lineShape1.BorderWidth = 3;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 10;
            this.lineShape1.X2 = 340;
            this.lineShape1.Y1 = 92;
            this.lineShape1.Y2 = 92;
            // 
            // textBoxResults
            // 
            this.textBoxResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxResults.Location = new System.Drawing.Point(13, 33);
            this.textBoxResults.Multiline = true;
            this.textBoxResults.Name = "textBoxResults";
            this.textBoxResults.ReadOnly = true;
            this.textBoxResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxResults.Size = new System.Drawing.Size(204, 334);
            this.textBoxResults.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Result";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "txt files | *.txt  |  all files | *.*";
            // 
            // buttonClr
            // 
            this.buttonClr.Location = new System.Drawing.Point(286, 9);
            this.buttonClr.Name = "buttonClr";
            this.buttonClr.Size = new System.Drawing.Size(55, 20);
            this.buttonClr.TabIndex = 26;
            this.buttonClr.Text = "Clear";
            this.buttonClr.UseVisualStyleBackColor = true;
            this.buttonClr.Click += new System.EventHandler(this.buttonClr_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 386);
            this.Controls.Add(this.splitContainer);
            this.MinimumSize = new System.Drawing.Size(608, 424);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Virtual Memory";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button buttonHandle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPages;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxAllAccess;
        private System.Windows.Forms.TextBox textBoxFaults;
        private System.Windows.Forms.TextBox textBoxResults;
        private System.Windows.Forms.Button buttonGenOnePage;
        private System.Windows.Forms.Button buttonGenSeverlaPages;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radioButtonLRU;
        private System.Windows.Forms.RadioButton radioButtonRandom;
        private System.Windows.Forms.RadioButton radioButtonClock;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonInit;
        private System.Windows.Forms.TextBox textBoxPrMemSz;
        private System.Windows.Forms.TextBox textBoxMemSz;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxRandomFaults;
        private System.Windows.Forms.TextBox textBoxClockFaults;
        private System.Windows.Forms.TextBox textBoxLRUFaults;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonFile;
        private System.Windows.Forms.Button buttonClr;
    }
}

