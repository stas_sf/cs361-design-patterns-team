﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VirtualMemoryLib;
using VirtualMemoryLib.Algorithms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace VirtualMemoryInterface
{
    public partial class MainForm : Form
    {
        private HashSet<char> allowableSymbols = new HashSet<char> {'1','2','3','4','5','6','7','8','9','0',' '};
        private VirtualMemory memory;
        private Random rand = new Random();
        private Request request = null;
        private LRURequest lru;
        private RandomRequest randomRequest;
        private ClockRequest clock;
        private int faults = 0, access = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonGenOnePage_Click(object sender, EventArgs e)
        {
            int r = rand.Next() % memory.MemorySize;
            textBoxPages.AppendText(' '+r.ToString());
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            InitVirtualMemory(10, 5);
        }

        private void InitVirtualMemory(int memSz, int PrimMemSz)
        {
            memory = new VirtualMemory(memSz, PrimMemSz);
            lru = new LRURequest(memory);
            randomRequest = new RandomRequest(memory);
            clock = new VirtualMemoryLib.Algorithms.ClockRequest(memory);
        }

        private void buttonGenSeverlaPages_Click(object sender, EventArgs e)
        {
            string s = "";
            int n = rand.Next() % 10;
            for (int i = 0; i < n; ++i)
            {
                int r = rand.Next() % memory.MemorySize;
                s += ' ' + r.ToString();
            }
            textBoxPages.AppendText(s);
        }

        private void LoadFromFile(string path)
        {
            string[] strs = File.ReadAllLines(path);
            string str = "";
            foreach (string s in strs)
            {
                str += s + ' ';
            }
            textBoxPages.Text = str;
        }

        private void buttonHandle_Click(object sender, EventArgs e)
        {
            /*if (textBoxPages.Text.Length == 0) return;
            string t = textBoxPages.Text;
            if (!allowableSymbols.Contains(t[t.Length - 1]))
            {
                MessageBox.Show("enter just digits");
                textBoxPages.Select(t.Length - 1, 0);
                return;
            }*/
            

            panel1.Enabled = false;
            try
            {
                List<int> pages = ParsePagesString(textBoxPages.Text);
                if (request == null)
                {
                    throw new System.NullReferenceException();
                }
                foreach (int p in pages)
                {
                    RequestLog rl = memory.MakeRequest(request, p);
                    Thread.Sleep(300);
                    textBoxResults.AppendText(rl.ToString() + "\r\n\r\n");
                    ++access;
                    if (!rl.Success) { ++faults; }
                }
                textBoxAllAccess.Text = access.ToString();
                textBoxFaults.Text = faults.ToString();
            }
            catch (System.FormatException err)
            { MessageBox.Show("Enter numbers of pages!"); panel1.Enabled = true; }
            catch (System.NullReferenceException err)
            { MessageBox.Show("Choose algorithm!"); panel1.Enabled = true; }
            catch (WrongPageException exc)
            {
                MessageBox.Show("Wrong number of some pages!"); panel1.Enabled = true;
            }
            
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            textBoxResults.Text = "";
            textBoxFaults.Text = "";
            textBoxAllAccess.Text = "";
            InitVirtualMemory(10, 5);
        }

        private List<int> ParsePagesString(string str)
        {
            List<int> l = new List<int>();
            char[] space = { ' ' };
            string[] nums = str.Split(space,StringSplitOptions.RemoveEmptyEntries);
            bool b;
            int a;
            for (int i = 0; i < nums.Length; ++i)
            {
                b = int.TryParse(nums[i], out a);
                if (!b)
                    throw new System.FormatException();
                l.Add(a);
            }
            return l;
        }

        private void radioButtonRandom_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonClock.Checked)
                request = clock;
            else
                if (radioButtonLRU.Checked)
                    request = lru;
                else
                    if (radioButtonRandom.Checked)
                        request = randomRequest;
        }


        private void buttonInit_Click(object sender, EventArgs e)
        {
            int memSz, primMemSz;
            bool b1 = int.TryParse(textBoxMemSz.Text, out memSz);
            bool b2 = int.TryParse(textBoxPrMemSz.Text, out primMemSz);
            if (!b1 || !b2 || primMemSz >= memSz)
            {
                MessageBox.Show("Enter just numbers (Primary memory size < Memory size)");
                return;
            }
            InitVirtualMemory(memSz, primMemSz);
            MessageBox.Show("New memory was initialized");
        }

        private string PrintTestInfo(int access, int faults)
        {
            if (access == 0) throw new System.DivideByZeroException();
            double perc = 100 * (double)faults / (double)access;
            return string.Format("{0}/{1} ({2:F}%)",faults,access,perc);
        }

        private void Test(out int LRUFaults, out int RandFaults, out int ClockFaults, out int pagesCount)
        {
            radioButtonClock.Checked = false;
            radioButtonLRU.Checked = false;
            radioButtonRandom.Checked = false;
            request = lru;
            LRUFaults = 0; RandFaults = 0; ClockFaults = 0;
            pagesCount = 0;
            try
            {
                List<int> pages = ParsePagesString(textBoxPages.Text);
                pagesCount = pages.Count;
                foreach (int p in pages)
                {
                    RequestLog rl = request.MakeRequest(p);
                    if (!rl.Success) { ++LRUFaults; }
                }
                request = clock;
                foreach (int p in pages)
                {
                    RequestLog rl = request.MakeRequest(p);
                    if (!rl.Success) { ++ClockFaults; }
                }
                request = randomRequest;
                foreach (int p in pages)
                {
                    RequestLog rl = request.MakeRequest(p);
                    if (!rl.Success) { ++RandFaults; }
                }
                request = null;
            }
            catch (System.FormatException err) { MessageBox.Show("Wrong number of page"); }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            int l, r, c, pc;
            Test(out l, out r, out c, out pc);
            try
            {
                textBoxLRUFaults.Text = PrintTestInfo(pc, l);
                textBoxRandomFaults.Text = PrintTestInfo(pc, r);
                textBoxClockFaults.Text = PrintTestInfo(pc, c);
            }
            catch (System.FormatException err)
            { MessageBox.Show("Enter just numbers"); }
            catch (System.DivideByZeroException err)
            { MessageBox.Show("Enter page numbers to begin testing"); }
        }

        private void buttonFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                LoadFromFile(openFileDialog1.FileName);
        }

        private void buttonClr_Click(object sender, EventArgs e)
        {
            textBoxPages.Text = "";
        }

        /*private void textBoxPages_TextChanged(object sender, EventArgs e)
        {
            if (textBoxPages.Text.Length == 0) return;
            string t = textBoxPages.Text;
            if (!allowableSymbols.Contains(t[t.Length - 1]))
            {
                MessageBox.Show("enter just digits");
                textBoxPages.Select(t.Length - 1, 0);
                return;
            }
        }*/
    }
}
