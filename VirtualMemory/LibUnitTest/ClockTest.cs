﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VirtualMemoryLib;
using VirtualMemoryLib.Algorithms;

namespace LibUnitTest
{
    [TestClass]
    public class ClockTest
    {
        [TestMethod]
        public void ClockTest1()
        {
            VirtualMemory[] vm = new VirtualMemory[3];

            vm[0] = new VirtualMemory(5, 5);
            vm[1] = new VirtualMemory(0, 0);
            vm[2] = new VirtualMemory(40, 40);

            for (int j = 0; j < vm.Length; ++j)
            {
                ClockRequest l = new ClockRequest(vm[j]);
                for (int i = 0; i < vm[j].MemorySize; ++i)
                    Assert.AreEqual(true, l.MakeRequest(i).Success);
            }
        }

        [TestMethod]
        public void ClockTest2()
        {
            VirtualMemory[] vm = new VirtualMemory[6];

            vm[0] = new VirtualMemory(5, 1);
            vm[1] = new VirtualMemory(40, 1);
            vm[2] = new VirtualMemory(1, 1);
            vm[3] = new VirtualMemory(2, 2);
            vm[4] = new VirtualMemory(5, 2);
            vm[5] = new VirtualMemory(40, 2);

            for (int j = 0; j < 3; ++j)
            {
                ClockRequest l = new ClockRequest(vm[j]);
                Assert.AreEqual(true, l.MakeRequest(0).Success);
                for (int i = 1; i < vm[j].MemorySize; ++i)
                    Assert.AreEqual(false, l.MakeRequest(i).Success);
            }

            for (int j = 3; j < vm.Length; ++j)
            {
                ClockRequest l = new ClockRequest(vm[j]);
                Assert.AreEqual(true, l.MakeRequest(0).Success);
                Assert.AreEqual(true, l.MakeRequest(1).Success);
                for (int i = 2; i < vm[j].MemorySize; ++i)
                    Assert.AreEqual(false, l.MakeRequest(i).Success);
            }
        }

        [TestMethod]
        public void ClockTest3()
        {
            VirtualMemory vm = new VirtualMemory(10, 5);

            ClockRequest l = new ClockRequest(vm);

            RequestLog r;

            r = l.MakeRequest(0);
            Assert.AreEqual(true, r.Success);

            r = l.MakeRequest(3);
            Assert.AreEqual(true, r.Success);

            r = l.MakeRequest(7);
            Assert.AreEqual(false, r.Success);
            Assert.AreEqual(0, r.ReplacedPage);

            r = l.MakeRequest(2);
            Assert.AreEqual(true, r.Success);

            r = l.MakeRequest(4);
            Assert.AreEqual(true, r.Success);

            r = l.MakeRequest(0);
            Assert.AreEqual(false, r.Success);
            Assert.AreEqual(1, r.ReplacedPage);

            r = l.MakeRequest(6);
            Assert.AreEqual(false, r.Success);
            Assert.AreEqual(2, r.ReplacedPage);

            r = l.MakeRequest(4);
            Assert.AreEqual(true, r.Success);

            r = l.MakeRequest(1);
            Assert.AreEqual(false, r.Success);
            Assert.AreEqual(3, r.ReplacedPage);


        }
    }
}
