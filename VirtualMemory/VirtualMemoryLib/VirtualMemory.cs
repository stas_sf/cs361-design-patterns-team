﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualMemoryLib.Algorithms;

namespace VirtualMemoryLib
{
    public class VirtualMemory
    {
        //Виртуальная память
        public List<int> memory;

        public int MemorySize { get { return memory.Capacity; } }
        public int PrimaryMemorySize { get { return primarySize; } }

        //Первчиная память
        private int primarySize;

        public VirtualMemory(int memorySize, int primarySize)
        {
            if (memorySize < 0 || primarySize < 0)
                throw new ArgumentException("Размер должен быть неотрицательным!");
            if (primarySize > memorySize)
                throw new ArgumentOutOfRangeException("Размер первичной памяти должен быть не больше общего размера памяти");
            memory = new List<int>(memorySize);
            this.primarySize = primarySize;
            for (int i = 0; i < memorySize; ++i)
                memory.Add(i);
        }

        public RequestLog MakeRequest(Request r, int i)
        {
            return r.MakeRequest(i);
        }
    }
}
