﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    public class ClockRequest : Request
    {
        List<int> clocks;
        int cur;

        public ClockRequest(VirtualMemory vm): base(vm)
        {
            int c = vm.PrimaryMemorySize;
            clocks = new List<int>();
            for (int j = 0; j < c; ++j)
                clocks.Add(1);
            cur = 0;
        }

        public override RequestLog MakeRequest(int i)
        {
            int new_ind = -1;
            bool s = true;

            int ind = Check(i);
            if (ind == -1)
                throw new WrongPageException("Страница не находится в вирт. памяти.");

            if (ind >= memory.PrimaryMemorySize)
            {
                s = false;
                new_ind = FindNewAdress();
                int t = memory.memory[new_ind];
                memory.memory[new_ind] = memory.memory[ind];
                memory.memory[ind] = t;
            }
            if (s)
                return new RequestLog(s, i, -1);
            else return new RequestLog(s, i, memory.memory[ind]);
        }

        public override int FindNewAdress()
        {
            while (true)
            {
                int res;
                while (clocks[cur] != 0)
                {
                    clocks[cur] = 0;
                    cur = (cur + 1) % memory.PrimaryMemorySize;
                }
                res =  cur;
                clocks[cur] = 1;
                cur = (cur + 1) % memory.PrimaryMemorySize;
                return res;
                    
            }
        }
    }
}
