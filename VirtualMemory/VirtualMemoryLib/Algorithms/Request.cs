﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    public abstract class Request
    {
        protected VirtualMemory memory;

        public Request(VirtualMemory vm)
        {
            memory = vm;
        }

        public int Check(int i)
        {
            int ind = -1;
            for (int j = 0; j < memory.memory.Count; ++j)
                if (memory.memory[j] == i)
                {
                    ind = j;
                    break;
                }
            return ind;
        }

        public abstract RequestLog MakeRequest(int i);
        public abstract int FindNewAdress();
    }
}
