﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    public class LRURequest : Request
    {
        private int[] counter;
        int timer;

        public LRURequest(VirtualMemory vm)
            : base(vm)
        {
            timer = 0;
            counter = new int[vm.MemorySize];
            for (int i = 0; i < counter.Length; ++i)
                counter[i] = 0;
        }

        public override RequestLog MakeRequest(int i)
        {
            ++timer;
            int new_ind = -1;
            bool s = true;

            int ind = Check(i);
            if (ind == -1)
                throw new WrongPageException("Страница не находится в вирт. памяти.");

            counter[i] = timer;

            if (ind >= memory.PrimaryMemorySize)
            {
                s = false;
                new_ind = FindNewAdress();
                int t = memory.memory[new_ind];
                memory.memory[new_ind] = memory.memory[ind];
                memory.memory[ind] = t;
            }
            if (s)
                return new RequestLog(s, i, -1);
            else return new RequestLog(s, i, memory.memory[ind]);
        }

        public override int FindNewAdress()
        {
            int minIndex = memory.memory[0];
            int min = counter[memory.memory[0]];
            for (int i = 1; i < counter.Length; ++i)
            {
                int page = -1;
                for (int j = 0; j < memory.PrimaryMemorySize; ++j)
                    if (memory.memory[j] == i)
                        page = j;
                if (min > counter[i] && page != -1)
                {
                    min = counter[i];
                    minIndex = i;
                }
            }
            for (int i = 0; i < memory.MemorySize; ++i)
                if (minIndex == memory.memory[i])
                    return i;
            throw new Exception();
        }
    }
}
