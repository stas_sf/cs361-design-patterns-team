﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    [Serializable()]
    public class WrongPageException : System.Exception
    {
        public WrongPageException() : base() { }
        public WrongPageException(string message) : base(message) { }
        public WrongPageException(string message, System.Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected WrongPageException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }
}
