﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    public class RandomRequest : Request
    {
        public RandomRequest(VirtualMemory vm) : base(vm) { }

        public override RequestLog MakeRequest(int i)
        {
            int new_ind = -1;
            bool s = true;

            int ind = Check(i);
            if (ind == -1)
                throw new WrongPageException("Страница не находится в вирт. памяти.");

            if (ind >= memory.PrimaryMemorySize)
            {
                s = false;
                new_ind = FindNewAdress();
                int t = memory.memory[new_ind];
                memory.memory[new_ind] = memory.memory[ind];
                memory.memory[ind] = t;
            }
            if (s)
                return new RequestLog(s, i, -1);
            else return new RequestLog(s, i, memory.memory[ind]);
        }

        public override int FindNewAdress()
        {
            Random r = new Random();
            int new_ind = r.Next(memory.PrimaryMemorySize);
            return new_ind;
        }
    }
}
