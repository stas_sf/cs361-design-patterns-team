﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualMemoryLib.Algorithms
{
    public class RequestLog
    {
        bool success;
        int page;
        int replacedPage;
        public RequestLog(bool s, int p, int rp)
        {
            success = s;
            page = p;
            replacedPage = rp;
        }

        public bool Success { get { return success; } }
        public int Page { get { return page; } }
        public int ReplacedPage { get { return replacedPage; } }

        public override string ToString()
        {
            if (success)
                return string.Format(" Запрос страницы #{0}: \r\n   * успех", page);
            else
                return string.Format(" Запрос страницы #{0}:\r\n   * промах\r\n   * замещена страница #{1}", page, replacedPage);

        }
    }
}
