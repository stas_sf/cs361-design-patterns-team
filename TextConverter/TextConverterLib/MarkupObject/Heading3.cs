﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupObject
{
    class Heading3 : AbstractMarkupObject
    {
        private string text;

        public Heading3(string t)
        {
            text = t;
        }

        public string Text { get { return text; } }
    }
}
