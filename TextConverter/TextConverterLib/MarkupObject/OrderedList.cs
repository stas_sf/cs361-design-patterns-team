﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupObject
{
    class OrderedList: AbstractMarkupObject
    {
        private List<string> list;

        public OrderedList(List<string> l)
        {
            list = l;
        }

        public List<string> List { get { return list; } }
    }
}
