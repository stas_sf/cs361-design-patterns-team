﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupObject
{
    class BulletList : AbstractMarkupObject
    {
        private List<string> list;

        public BulletList(List<string> l)
        {
            list = l;
        }

        public List<string> List { get { return list; } }
    }
}
