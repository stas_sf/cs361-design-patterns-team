﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupObject
{
    class Paragraph: AbstractMarkupObject
    {
        private string text;

        public Paragraph(string t)
        {
            text = t;
        }

        public string Text { get { return text; } }
    }
}
