﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupObject
{
    class Heading1 : AbstractMarkupObject
    {
        private string text;

        public Heading1(string t)
        {
            text = t;
        }

        public string Text { get { return text; } }
    }
}
