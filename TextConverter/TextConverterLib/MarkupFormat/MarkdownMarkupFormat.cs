﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextConverterLib;

namespace TextConverterLib.MarkupFormat
{
    class MarkdownMarkupFormat : AbstractMarkupFormat
    {
        public MarkdownMarkupFormat()
            : base(Constants.MarkdownMarkupFormat.paragraph, Constants.MarkdownMarkupFormat.heading1, Constants.MarkdownMarkupFormat.heading2,
            Constants.MarkdownMarkupFormat.heading3, Constants.MarkdownMarkupFormat.orderedlistStart, Constants.MarkdownMarkupFormat.orderedlistBullet,
            Constants.MarkdownMarkupFormat.bulletlistStart, Constants.MarkdownMarkupFormat.bulletlistBullet) { }
    }
}
