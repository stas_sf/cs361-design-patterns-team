﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextConverterLib;

namespace TextConverterLib.MarkupFormat
{
    class HTMLMarkupFormat: AbstractMarkupFormat
    {
        public HTMLMarkupFormat()
            : base(Constants.HTMLMarkupFormat.paragraph, Constants.HTMLMarkupFormat.heading1, Constants.HTMLMarkupFormat.heading2,
            Constants.HTMLMarkupFormat.heading3, Constants.HTMLMarkupFormat.orderedlistStart, Constants.HTMLMarkupFormat.orderedlistBullet,
            Constants.HTMLMarkupFormat.bulletlistStart, Constants.HTMLMarkupFormat.bulletlistBullet) { }
    }
}
