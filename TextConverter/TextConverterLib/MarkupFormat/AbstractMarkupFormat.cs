﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib.MarkupFormat
{
    public abstract class AbstractMarkupFormat
    {
        private string
            paragraph,
            heading1,
            heading2,
            heading3,
            orderedlistStart,
            orderedlistBullet,
            bulletlistStart,
            bulletlistBullet;

        protected AbstractMarkupFormat(string p = "", string h1 = "", string h2 = "",
            string h3 = "", string ols = "", string olb = "", string bls = "",string blb = "")
        {
            paragraph = p;
            heading1 = h1; 
            heading2 = h2;
            heading3 = h3;
            orderedlistStart = ols;
            orderedlistBullet = olb;
            bulletlistStart = bls;
            bulletlistBullet = blb;
        }
    
        public string Paragraph { get {return paragraph;}}
        public string Heading1 { get { return heading1; } }
        public string Heading2 { get { return heading2; } }
        public string Heading3 { get { return heading3; } }
        public string OrderedlistStart { get { return orderedlistStart; } }
        public string OrderedlistBullet { get { return orderedlistBullet; } }
        public string BulletlistStart { get { return bulletlistStart; } }
        public string BulletlistBullet { get { return bulletlistBullet; } }

    }
}
