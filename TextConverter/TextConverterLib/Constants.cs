﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextConverterLib
{
    class Constants
    {
        public class HTMLMarkupFormat
        {
            public const string paragraph = "<p>{0}</p>",  
            heading1 = "<h1>{0}</h1>",
            heading2 = "<h2>{0}</h2>",
            heading3 = "<h3>{0}</h3>",
            orderedlistStart = "<ol>\r\n{0}</ol>\r\n\r\n",
            orderedlistBullet = "<li>{0}</li>\r\n",
            bulletlistStart = "<ul>\r\n{0}</ul>\r\n\r\n",
            bulletlistBullet = "<li>{0}</li>\r\n";
        }

        public class MarkdownMarkupFormat
        {
            public const string paragraph = "\n\n{0}",
            heading1 = "#{0}\n\n",
            heading2 = "##{0}\n\n",
            heading3 = "###{0}\n\n",
            orderedlistStart = "\r\n{0}\r\n",
            orderedlistBullet = "1.{0}",
            bulletlistStart = "\r\n{0}\r\n",
            bulletlistBullet = "*{0}";
        }
        
        public class StrangeMarkup
        {
            public const string paragraph = "p",
            heading1 = "h1",
            heading2 = "h2",
            heading3 = "h3",
            //костыль 1 (в строке либо только ключевое слово ordlist (orderedlistStart0), ordlist и еще что-то (orderedlistStart1))
            orderedlistStart0 = "ordlist\r",
            orderedlistStart1 = "ordlist",
            orderedlistBullet = "",
            //костыль 2 (аналогично костыль 1, только для bullist)
            bulletlistStart0 = "bullist\r",
            bulletlistStart1 = "bullist",
            bulletlistBullet = "";
        }
    }
}
