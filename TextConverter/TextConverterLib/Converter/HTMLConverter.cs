﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextConverterLib;
using TextConverterLib.MarkupFormat;

namespace TextConverterLib.Converter
{
    public class HTMLConverter : AbstractConverter
    {
        public HTMLConverter() : base(new HTMLMarkupFormat()) { }
    }
}
