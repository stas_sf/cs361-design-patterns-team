﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextConverterLib.MarkupFormat;
using TextConverterLib.MarkupObject;

namespace TextConverterLib.Converter
{
    public abstract class AbstractConverter
    {
        /// <summary>
        /// Предоставляет форматные строчки для конкретного формата
        /// </summary>
        private AbstractMarkupFormat mf;
        private const string EndlFormat = "\r\n\r\n";
        private const string space = " ";

        /// <summary>
        /// Конвертор текста
        /// </summary>
        /// <param name="mafo"></param>
        public AbstractConverter(AbstractMarkupFormat mafo = null)
        {
            mf = mafo;
        }

        /// <summary>
        /// Конвертирует текст в "странном" формате в заданный формат
        /// </summary>
        /// <param name="text">Конвертируемый текст</param>
        /// <returns></returns>
        public string Convert(string text)
        {
            StringBuilder sb = new StringBuilder();

            List<AbstractMarkupObject> mobjects = GetMarkupObjects(text);


            for (int i = 0; i < mobjects.Count; ++i)
                if (mobjects[i].GetType() == typeof(Paragraph))
                {
                    Paragraph p = mobjects[i] as Paragraph;
                    sb.Append(ConvertParagraph(p));
                }

                else if (mobjects[i].GetType() == typeof(Heading1))
                {
                    Heading1 h1 = mobjects[i] as Heading1;
                    sb.Append(ConvertHeading1(h1));
                }
                else if (mobjects[i].GetType() == typeof(Heading2))
                {
                    Heading2 h2 = mobjects[i] as Heading2;
                    sb.Append(ConvertHeading2(h2));
                }
                else if (mobjects[i].GetType() == typeof(Heading3))
                {
                    Heading3 h3 = mobjects[i] as Heading3;
                    sb.Append(ConvertHeading3(h3));
                }
                else if (mobjects[i].GetType() == typeof(OrderedList))
                {
                    OrderedList ol = mobjects[i] as OrderedList;
                    sb.Append(ConvertOrderedList(ol));
                }
                else if (mobjects[i].GetType() == typeof(BulletList))
                {
                    BulletList bl = mobjects[i] as BulletList;
                    sb.Append(ConvertBulletList(bl));
                }

            return sb.ToString();
        }

        /// <summary>
        /// Преобразует абзац в форматированный абзац
        /// </summary>
        /// <param name="p">абзац</param>
        /// <returns></returns>
        internal virtual string ConvertParagraph(Paragraph p)
        {
            return String.Format(mf.Paragraph + EndlFormat, p.Text);
        }

        /// <summary>
        /// Преобразует заголовок первого уровня в форматированный заголовок первого уровня
        /// </summary>
        /// <param name="h1">заголовок первого уровня</param>
        /// <returns></returns>
        internal virtual string ConvertHeading1(Heading1 h1)
        {
            return String.Format(mf.Heading1 + EndlFormat, h1.Text);
        }

        /// <summary>
        /// Преобразует заголовок второго уровня в форматированный заголовок второго уровня
        /// </summary>
        /// <param name="h2">заголовок второго уровня</param>
        /// <returns></returns>
        internal virtual string ConvertHeading2(Heading2 h2)
        {
            return String.Format(mf.Heading2 + EndlFormat, h2.Text);
        }

        /// <summary>
        /// Преобразует заголовок третьего уровня в форматированный заголовок третьего уровня
        /// </summary>
        /// <param name="h3">заголовок третьего уровня</param>
        /// <returns></returns>
        internal virtual string ConvertHeading3(Heading3 h3)
        {
            return String.Format(mf.Heading3 + EndlFormat, h3.Text);
        }

        /// <summary>
        /// Преобразует нумерованный список в форматированный нумерованный список
        /// </summary>
        /// <param name="ol">нумерованный список</param>
        /// <returns></returns>
        internal virtual string ConvertOrderedList(OrderedList ol)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ol.List.Count; ++i)
                sb.AppendFormat(mf.OrderedlistBullet + "\n", ol.List[i]);
            return String.Format(mf.OrderedlistStart, sb.ToString());
        }

        /// <summary>
        /// Преобразует маркированный список в форматированный маркированный список
        /// </summary>
        /// <param name="bl">маркированный список</param>
        /// <returns></returns>
        internal virtual string ConvertBulletList(BulletList bl)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bl.List.Count; ++i)
                sb.AppendFormat(mf.BulletlistBullet + "\n", bl.List[i]);
            return String.Format(mf.BulletlistStart, sb.ToString());
        }

        /// <summary>
        /// На основе текста в "странном" формате создаёт массив обектов
        /// объектов, готовых к преобразованию
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        internal List<AbstractMarkupObject> GetMarkupObjects(string text)
        {
            List<AbstractMarkupObject> l = new List<AbstractMarkupObject>();

            String[] StrArr = text.Split(new string[] { EndlFormat }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < StrArr.Length; ++i )
            {
                String[] units = StrArr[i].Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                int j = 0;
                while (j < units.Length)
                {
                    String[] strs = units[j].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    StringBuilder sb = new StringBuilder(null);
                    switch (strs[0])
                    {
                        case Constants.StrangeMarkup.paragraph:
                            for (int k = 1; k < strs.Length; ++k)
                                sb.Append(strs[k] + space);
                            l.Add(new Paragraph(sb.ToString()));
                            break;
                        case Constants.StrangeMarkup.heading1:
                            for (int k = 1; k < strs.Length; ++k)
                                sb.Append(strs[k]+space);
                            l.Add(new Heading1(sb.ToString()));
                            break;
                        case Constants.StrangeMarkup.heading2:
                            for (int k = 1; k < strs.Length; ++k)
                                sb.Append(strs[k] + space);
                            l.Add(new Heading2(sb.ToString()));
                            break;
                        case Constants.StrangeMarkup.heading3:
                            for (int k = 1; k < strs.Length; ++k)
                                sb.Append(strs[k] + space);
                            l.Add(new Heading3(sb.ToString()));
                            break;
                        case Constants.StrangeMarkup.orderedlistStart0:
                        case Constants.StrangeMarkup.orderedlistStart1:
                            List<string> ls = new List<string>();
                            {
                                ++j;
                                while (j < units.Length)
                                {
                                    ls.Add(units[j]);
                                    ++j;
                                }
                            }
                            l.Add(new OrderedList(ls));
                            break;
                        case Constants.StrangeMarkup.bulletlistStart0:
                        case Constants.StrangeMarkup.bulletlistStart1:
                            List<string> ls1 = new List<string>();
                            {
                                ++j;
                                while (j < units.Length)
                                {
                                    ls1.Add(units[j]);
                                    ++j;
                                }
                            }
                            l.Add(new BulletList(ls1));
                            break;
                        default: break;
                    }
                    ++j;
                }
                
            }

            return l;
        }

    }
}
