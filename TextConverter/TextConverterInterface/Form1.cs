﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TextConverterLib;
using TextConverterLib.Converter;

namespace TextConverterInterface
{
    public partial class Form1 : Form
    {
        string file;
        private TextConverterLib.Converter.AbstractConverter converter;
        private TextConverterLib.Converter.HTMLConverter htmlConverter;
        private TextConverterLib.Converter.MarkdownConverter markdownConverter;

        public Form1()
        {
            InitializeComponent();
            htmlConverter = new HTMLConverter();
            markdownConverter = new MarkdownConverter();
            converter = htmlConverter;
            
        }

        private void bitbucketWikiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/RastaStas/cs361-design-patterns-team/wiki/Home");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                file = openFileDialog1.FileName;
                string text = File.ReadAllText(file, Encoding.Default);
                textBox1.Text = text;
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Text|*.txt|HTML|*.html|Markdown|*.markdown";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog1.FileName != "")
                {
                    File.WriteAllText(saveFileDialog1.FileName, textBox2.Text);
                }
            }
        }

        private void textHtmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textMarkdownToolStripMenuItem.Checked = false;
            textHtmlToolStripMenuItem.Checked = true;
            converter = htmlConverter;
        }

        private void textMarkdownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textHtmlToolStripMenuItem.Checked = false;
            textMarkdownToolStripMenuItem.Checked = true;
            converter = markdownConverter;
        }

        private void convertToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string text = textBox1.Text;
            textBox2.Text = converter.Convert(text);
        }
    }
}
